package com.smtech.socialdownloader;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;

import java.util.Arrays;
import java.util.Date;

import static androidx.lifecycle.Lifecycle.Event.ON_START;

public class AppOpenManager implements Application.ActivityLifecycleCallbacks, LifecycleObserver {
    private static final String TAG = "AppOpenManager";
    private AppOpenAd appOpenAd = null;
    private long loadTime = 0;

    private AppOpenAd.AppOpenAdLoadCallback loadCallback;
    private Activity currentActivity;
    private static boolean isShowingAd = false;

    private final Application myApplication;

    /**
     * Constructor
     */
    public AppOpenManager(Application myApplication) {
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    /**
     * Request an ad
     */
    public void fetchAd(AdConfig adConfig) {
        // Have unused ad, no need to fetch another.
        if (isAdAvailable()) {
            return;
        }

        loadCallback =
                new AppOpenAd.AppOpenAdLoadCallback() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.e(TAG, "onAdFailedToLoad: " + loadAdError.getMessage());
                    }

                    @Override
                    public void onAdLoaded(@NonNull AppOpenAd appOpenAd) {
                        super.onAdLoaded(appOpenAd);
                        AppOpenManager.this.appOpenAd = appOpenAd;
                        FirebaseAnalyticsUtils.adLoaded(currentActivity, Arrays.toString(currentActivity.getClass().getSimpleName().split("Activity")), AppConstant.admobAppOpen);
                        loadTime = (new Date()).getTime();
                    }

                };
        AdRequest request = getAdRequest();
        String adId = adConfig.getAdProvider().getAdmobAppOpen() == null ? currentActivity.getApplicationContext().getString(R.string.admob_app_open) : adConfig.getAdProvider().getAdmobAppOpen();
        AppOpenAd.load(
                myApplication, adId, request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
    }

    /**
     * Shows the ad if one isn't already showing.
     */
    public void showAdIfAvailable() {
        // Only show ad if there is not already an app open ad currently showing
        // and an ad is available.
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(currentActivity);
        if (adConfig != null) {
            if (adConfig.getAdProvider().getAdStatus() == 1 && !adConfig.getAdProvider().getAdProvider().equalsIgnoreCase(AppConstant.facebook) && adConfig.getAdProvider().getIsAppOpen() == 1) {
                if (!isShowingAd && isAdAvailable()) {
                    Log.e(TAG, "showAdIfAvailable: 123");

                    FullScreenContentCallback fullScreenContentCallback =
                            new FullScreenContentCallback() {
                                @Override
                                public void onAdDismissedFullScreenContent() {
                                    // Set the reference to null so isAdAvailable() returns false.
                                    AppOpenManager.this.appOpenAd = null;
                                    isShowingAd = false;
                                    fetchAd(adConfig);
                                }

                                @Override
                                public void onAdFailedToShowFullScreenContent(AdError adError) {
                                }

                                @Override
                                public void onAdShowedFullScreenContent() {
                                    isShowingAd = true;
                                }

                                @Override
                                public void onAdClicked() {
                                    super.onAdClicked();
                                    FirebaseAnalyticsUtils.adClicked(currentActivity, Arrays.toString(currentActivity.getClass().getSimpleName().split("Activity")), AppConstant.admobAppOpen);
                                }

                                @Override
                                public void onAdImpression() {
                                    super.onAdImpression();
                                }
                            };

                    appOpenAd.setFullScreenContentCallback(fullScreenContentCallback);
                    appOpenAd.show(currentActivity);

                } else {
                    fetchAd(adConfig);
                }
            }
        }
    }

    /**
     * Creates and returns ad request.
     */
    private AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    /**
     * Utility method that checks if ad exists and can be shown.
     */
    public boolean isAdAvailable() {
        return appOpenAd != null;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
       /* currentActivity = activity;
        showAdIfAvailable();*/
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        currentActivity = null;

    }

    /**
     * LifecycleObserver methods
     */
    @OnLifecycleEvent(ON_START)
    public void onStart() {
        showAdIfAvailable();
        Log.e(TAG, "onStart: ");
    }

}