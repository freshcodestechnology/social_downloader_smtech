package com.smtech.socialdownloader;

import static com.smtech.socialdownloader.utils.prefloader.init;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;
import android.util.Base64;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FacebookAdUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    public static Activity activity;
    public static Timer timer = new Timer();
    public static int userTime = 0;
    AppOpenManager appOpenManager;
    public static boolean isShowingAd = false;
    BillingClient billingClient;
    Prefs prefs;


    public MyApplication() {
        super();
    }

    public static MyApplication c() {
        return mInstance;
    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initImageLoader(getApplicationContext());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        prefs = new Prefs(this);
        checkSubscription();


        AudienceNetworkAds.initialize(this);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(@NonNull InitializationStatus initializationStatus) {
            }
        });
        appOpenManager = new AppOpenManager(this);
        getHashKey();
        init(this);

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                userTime = userTime + 1;
                if (activity != null) {
                    AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
                    if (adConfig != null) {
                        if (adConfig.getAdProvider().getIsTimer() == 1) {
                            int time = adConfig.getAdProvider().getTimerInterval();
                            if (userTime % time == 0) {
                                if (AppUtils.isAdmobAvailable(activity)) {
                                    /// showAdmobInterstitialAd is an overloaded function with bool isIntervalAd flag to reset the user timer variable.
                                    AdmobAdUtils.showAdmobInterstitialAd(activity, false, false);
                                } else {
                                    FacebookAdUtils.showFbInterstitialAd(activity, false);
                                }
                            }
                        }

                    }
                }
            }
        }, 0, 1000);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context).threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);
    }

    public void getHashKey() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));

                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    @Override
    public void onTerminate() {
        timer.cancel();
        super.onTerminate();
    }

    void checkSubscription() {

        billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener((billingResult, list) -> {
        }).build();

        final BillingClient finalBillingClient = billingClient;
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingServiceDisconnected() {

            }
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    finalBillingClient.queryPurchasesAsync(BillingClient.SkuType.SUBS, (billingResult1, list) -> {
                        //this "list" will contain all the sub purchases.
                        /*if (billingResult1.getResponseCode() == BillingClient.BillingResponseCode.OK && list.size() > 0) {
                            prefs.setPremium(1);
                            Log.i("TAG", "onBillingSetupFinished: "+list.size());
                        } else if (list.size() == 0) {
                            prefs.setPremium(0);
                        }*/
                        if (billingResult1.getResponseCode() == BillingClient.BillingResponseCode.OK && list.size() > 0){
                            int i = 0;
                            for (Purchase purchase: list){
                                Log.i("TAG", "SKUSize" + purchase.getSkus().size());
                                if (purchase.getSkus().get(i).equals("01")) {
                                    Log.d("checkedonce", purchase + "");
                                    prefs.setPremium(1);
                                } else {
                                    prefs.setPremium(0);
                                }
                                i++;

                            }
                        }
                    });

                }
            }
        });
    }
}