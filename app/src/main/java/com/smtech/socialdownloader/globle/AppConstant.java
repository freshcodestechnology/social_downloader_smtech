package com.smtech.socialdownloader.globle;

public class AppConstant {
    public static final int SPLASH_TIME_OUT = 4000;
    public static String TikTokUrl = "http://codingdunia.com/ccprojects/tiktok/api/getContent/";
    public static String AD_CONFIG = "https://ads.freshcodes.in/api/appConfig/";
    public static String AD_DATA = "https://ads.freshcodes.in/api/appScreens/";

    //error messages
    public static final String NO_INTERNET_CONNECTION = "No internet connection";
    public static final String TIME_OUT = "Connection Timeout.Please try again.";
    public static final String SOMETHING_WRONG_TRY_AGAIN = "Something went wrong, Please try again!!";

    //preferences
    public static final String PREFS_NAME = "video_downloader";
    public static final String PREF_MEDIA_COUNT = "pref_media_count";
    public static final String PREF_AD_TYPE = "pref_ad_type";
    public static final String PREF_AD_CONFIG = "pref_ad_config";
    public static final String PREF_AD_DATA = "pref_ad_data";

    //ad constants
    public static final String admob = "Admob";
    public static final String facebook = "Facebook";

    /// these admob keys are pre-define in the documentation. so do must not update or change this keys.
    public static final String admobAppOpen = "admob_app_open";
    public static final String admobBanner = "admob_banner";
    public static final String admobNativeAdvanced = "admob_native_advanced";
    public static final String admobNativeAdvancedVideo = "admob_native_advanced_video";
    public static final String admobRewardedInterstitial = "admob_rewarded_interstitial";
    public static final String admobRewarded = "admob_rewarded";
    public static final String admobInterstitial = "admob_interstitial";
    public static final String admobInterstitialVideo = "admob_interstitial_video";

    /// these admob keys are pre-define in the documentation. so do must not update or change this keys.
    public static final String fbInterstitial = "fb_interstitial";
    public static final String fbRewarded = "fb_rewarded";
    public static final String fbBanner = "fb_banner";
    public static final String fbNative = "fb_native";

    /// third party ad constants
    public static String thirdPartyBanner = "third_party_banner";
}