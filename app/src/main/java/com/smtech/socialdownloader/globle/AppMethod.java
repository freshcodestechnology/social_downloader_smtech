package com.smtech.socialdownloader.globle;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.smtech.socialdownloader.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AppMethod {
    public static void setPreferenceObject(Context c, Object modal, String key) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String jsonObject = gson.toJson(modal);
        prefsEditor.putString(key, jsonObject);
        prefsEditor.commit();
    }

    public static Object getPreferenceObjectJson(Context c, String key, Class tClass) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());
        String json = appSharedPrefs.getString(key, "");
        Gson gson = new Gson();
        Object selectedUser = gson.fromJson(json, tClass);
        return selectedUser;
    }

    public static boolean setStringPreference(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getStringPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        String value = settings.getString(key, "");
        return value;
    }

    public static boolean setIntegerPreference(Context context, String key, int value) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static int getIntegerPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        int value = settings.getInt(key, -1);
        return value;
    }

    public static boolean setBooleanPreference(Context context, String key, Boolean value) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static Boolean getBooleanPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        Boolean value = settings.getBoolean(key, false);
        return value;
    }

    public static boolean setLongPreference(Context context, String key, long value) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLongPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        long value = settings.getLong(key, -1);
        return value;
    }

    public static boolean clearPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(AppConstant.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
        return editor.commit();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission")
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getString(String text, String defaultText) {
        if (!TextUtils.isEmpty(text)) {
            return text;
        }
        return defaultText;
    }

    public static void showInfoDialog(Context context, String title, String message, boolean cancelable, String
            positiveBtnTitle, String nagativeBtnTitle, DialogInterface.OnClickListener positiveButtonListener,
                                      DialogInterface.OnClickListener nagativeButtonListener) {
        try {
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing()) {
                    new AlertDialog.Builder(context)
                            .setTitle(title)
                            .setMessage(message)
                            .setCancelable(cancelable)
                            .setPositiveButton(positiveBtnTitle, positiveButtonListener)
                            .setNegativeButton(nagativeBtnTitle, nagativeButtonListener)
                            .show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAlert(Context context, String msg) {
        try {
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(context.getString(R.string.app_name));
                    alert.setMessage(msg);
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAlertWithOk(Context activity, String title, String msg) {
        try {
            if (activity instanceof Activity) {
                if (!((Activity) activity).isFinishing()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                    alert.setTitle(title);
                    alert.setMessage(msg);
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isJSONValid(String json) {
        try {
            new JSONObject(json);
        } catch (JSONException ex) {
            try {
                new JSONArray(json);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static String getUDID(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static String getCurrentDate(String dateFormat) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static File saveIamge(Context context, Bitmap finalBitmap) {
        String directoryName = context.getString(R.string.app_name);
        directoryName = directoryName.replace(" ", "");
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/" + directoryName);
        myDir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "Image-" + timeStamp + ".png";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isAllPermissionGranted(int[] grantResults) {
        if (grantResults.length == 0) {
            return true;
        } else {
            for (int element : grantResults) {
                if (element != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        }
    }

    public static Spanned fromHtml(String html) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return new SpannableString((TextUtils.isEmpty(result.toString()) ? "" : result.toString()));
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }

    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return TextUtils.isEmpty(type) ? "" : type;
    }
}