package com.smtech.socialdownloader;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.smtech.socialdownloader.R;

public class Constant {

    public static boolean isConnectedNetwork(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isAvailable() && activeNetwork.isConnected();
        }
        return false;
    }

    public static void NoInternetConnection(Activity activity){

        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_no_internet);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout((int) (DisplayMetricsHandler.getScreenWidth() - 50), Toolbar.LayoutParams.WRAP_CONTENT);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                activity.finish();
            }
        });

        TextView RefreshTextView = (TextView) dialog.findViewById(R.id.RefreshTextView);
        TextView OnDataTextView = (TextView) dialog.findViewById(R.id.OnDataTextView);

        dialog.show();

        RefreshTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isConnectedNetwork(activity)) {

                    Intent intent = new Intent(activity, activity.getClass());
                    activity.startActivity(intent);
                    activity.finish();

                    dialog.dismiss();

                } else {

                    Toast.makeText(activity, "Please Check Your Network Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        OnDataTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);

            }
        });
    }
}
