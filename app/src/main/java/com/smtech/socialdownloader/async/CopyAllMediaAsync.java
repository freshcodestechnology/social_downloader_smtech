package com.smtech.socialdownloader.async;

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;


import com.smtech.socialdownloader.Status;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.MediaModel;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;

public class CopyAllMediaAsync extends AsyncTask<Void, Void, Boolean> {
    Context context;
    ArrayList<com.smtech.socialdownloader.Status> mediaList;
    CopyMediaListener listener;
    AppProgressDialog appProgressDialog;

    File destinationDir;

    public CopyAllMediaAsync(Context context, ArrayList<com.smtech.socialdownloader.Status> mediaList, File destinationDir, CopyMediaListener listener) {
        this.context = context;
        this.mediaList = mediaList;
        this.destinationDir = destinationDir;
        this.listener = listener;
        appProgressDialog = new AppProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (appProgressDialog != null) {
            appProgressDialog.showDialog();
        }
        if (!destinationDir.exists()) {
            AppUtils.RootDirectoryWhatsAppShow.mkdir();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(context, FILE_PERMISSIONS)) {
            try {
                if (destinationDir != null && destinationDir.exists() && destinationDir.isDirectory()) {
                    if (mediaList != null && mediaList.size() > 0) {
                        for (int i = 0; i < mediaList.size(); i++) {
                            File f = new File(mediaList.get(i).getPath());
                            if (f != null && f.exists()) {
                                FileUtils.copyFileToDirectory(f, destinationDir);
                            }
                        }
                    }
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        if (appProgressDialog != null) {
            appProgressDialog.dismissDialog();
        }
        listener.onResult(success);
    }

    public interface CopyMediaListener {
        void onResult(boolean success);
    }
}