package com.smtech.socialdownloader.async;

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;


import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.MediaModel;
import com.smtech.socialdownloader.view.AppProgressDialog;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class GetAllFilesAsync extends AsyncTask<Void, Void, ArrayList<MediaModel>> {
    Context context;
    String directory;
    EventListener eventListener;

    AppProgressDialog appProgressDialog;

    public GetAllFilesAsync(Context context, String directory, EventListener eventListener) {
        this.context = context;
        this.directory = directory;
        this.eventListener = eventListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (appProgressDialog == null) {
            appProgressDialog = new AppProgressDialog(context);
        }
        if (appProgressDialog != null) {
            appProgressDialog.showDialog();
        }
    }

    @Override
    protected ArrayList<MediaModel> doInBackground(Void... voids) {
        ArrayList<MediaModel> mediaList = new ArrayList<>();
        try {
            String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (AppMethod.hasPermissions(context, FILE_PERMISSIONS)) {
                File dir = new File(Environment.getExternalStorageDirectory(), directory);
                if (dir != null && dir.exists() && dir.isDirectory()) {
                    File[] files = dir.listFiles();
                    if (files != null && files.length > 0) {
                        Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                        for (int i = 0; i < files.length; i++) {
                            if (files[i] != null && files[i].exists() && files[i].isFile() && (AppMethod.isImageFile(files[i].getPath()) || AppMethod.isVideoFile(files[i].getPath()))) {
                                mediaList.add(new MediaModel(files[i].getAbsolutePath()));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mediaList;
    }

    @Override
    protected void onPostExecute(ArrayList<MediaModel> mediaList) {
        super.onPostExecute(mediaList);
        if (appProgressDialog != null) {
            appProgressDialog.dismissDialog();
        }
        if (eventListener != null) {
            eventListener.onResult(mediaList);
        }
    }

    public interface EventListener {
        void onResult(ArrayList<MediaModel> mediaList);
    }
}