package com.smtech.socialdownloader.async;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;

import java.util.ArrayList;

public class InstaStartDownloadsAsync extends AsyncTask<Void, Void, Boolean> {
    Context context;
    String instaResponse;
    EventListener eventListener;
    AppProgressDialog appProgressDialog;
    DownloadManager manager;

    public InstaStartDownloadsAsync(Context context, String instaResponse, EventListener eventListener) {
        this.context = context;
        this.instaResponse = instaResponse;
        this.eventListener = eventListener;
        manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (appProgressDialog == null) {
            appProgressDialog = new AppProgressDialog(context);
        }
        if (appProgressDialog != null) {
            appProgressDialog.showDialog();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (AppMethod.hasPermissions(context, FILE_PERMISSIONS)) {
                ArrayList<String> downloadURLs = AppUtils.getInstaDownloadURLsFromResponse(instaResponse);
                if (downloadURLs != null && downloadURLs.size() > 0) {
                    for (int i = 0; i < downloadURLs.size(); i++) {
                        String downloadURL = downloadURLs.get(i);
                        String fileName = "";
                        if (downloadURL.contains(".mp4")) {
                            FirebaseAnalyticsUtils.statusDownload(context, true,"Instagram");
                            fileName = String.format("%s.mp4", AppMethod.getCurrentDate("yyyyMMddhhmmss"));
                        } else {
                            FirebaseAnalyticsUtils.statusDownload(context, false,"Instagram");
                            fileName = String.format("%s.jpg", AppMethod.getCurrentDate("yyyyMMddhhmmss"));
                        }
                        Uri uri = Uri.parse(downloadURL);
                        DownloadManager.Request request = new DownloadManager.Request(uri);
                        request.setTitle(context.getString(R.string.app_name));

                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, AppUtils.getInstagramDirPath  + fileName);
                        manager.enqueue(request);
                    }
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean b) {
        super.onPostExecute(b);
        if (appProgressDialog != null) {
            appProgressDialog.dismissDialog();
        }
        if (eventListener != null) {
            eventListener.onSuccess(b);
        }
    }

    public interface EventListener {
        void onSuccess(boolean success);
    }
}