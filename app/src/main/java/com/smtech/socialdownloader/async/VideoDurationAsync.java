package com.smtech.socialdownloader.async;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.webkit.URLUtil;

import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.view.AppProgressDialog;

import java.io.File;
import java.net.URLConnection;
import java.util.HashMap;

public class VideoDurationAsync extends AsyncTask<Void, Void, Long> {
    Context context;
    File videoFile;
    String videoURL;
    VideoDurationAsyncListener listener;

    boolean showProgress = false;
    AppProgressDialog progressDialog;

    int type = -1;

    public static final int TYPE_FROM_FILE = 1;
    public static final int TYPE_FROM_URL = 2;

    public VideoDurationAsync(Context context, File videoFile, VideoDurationAsyncListener listener) {
        this.context = context;
        this.videoFile = videoFile;
        this.listener = listener;
        this.type = TYPE_FROM_FILE;
    }

    public VideoDurationAsync(Context context, String videoURL, VideoDurationAsyncListener listener) {
        this.context = context;
        this.videoURL = videoURL;
        this.listener = listener;
        this.type = TYPE_FROM_URL;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) {
            if (progressDialog == null) {
                progressDialog = new AppProgressDialog(context);
            }
            if (progressDialog != null) {
                progressDialog.showDialog();
            }
        }
    }

    @Override
    protected Long doInBackground(Void... voids) {
        try {
            switch (type) {
                case TYPE_FROM_FILE:
                    if (videoFile != null &&
                            videoFile.exists() &&
                            videoFile.isFile() &&
                            AppMethod.getMimeType(videoFile.getAbsolutePath()).toLowerCase().contains("video")) {
                        return getDurationFromFile(videoFile);
                    }
                    break;
                case TYPE_FROM_URL:
                    if (!TextUtils.isEmpty(videoURL) && URLUtil.isValidUrl(videoURL) && isVideoFile(videoURL)) {
                        return getDurationFromURL(videoURL);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
        if (progressDialog != null) {
            progressDialog.dismissDialog();
        }
        if (aLong != null) {
            listener.onResult(true, aLong);
        } else {
            listener.onResult(false, 0);
        }
    }

    public long getDurationFromFile(File videoFile) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(context, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        retriever.release();
        return timeInMillisec;
    }

    public long getDurationFromURL(String videoURL) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(videoURL, new HashMap<String, String>());
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        retriever.release();
        return timeInMillisec;
    }

    public boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.toLowerCase().contains("video");
    }

    public boolean isShowProgress() {
        return showProgress;
    }

    public void setShowProgress(boolean showProgress) {
        this.showProgress = showProgress;
    }

    public interface VideoDurationAsyncListener {
        void onResult(boolean success, long timeInMillisec);
    }
}