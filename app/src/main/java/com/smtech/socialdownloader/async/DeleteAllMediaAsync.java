package com.smtech.socialdownloader.async;

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;

import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.MediaModel;
import com.smtech.socialdownloader.view.AppProgressDialog;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;

public class DeleteAllMediaAsync extends AsyncTask<Void, Void, Boolean> {
    Context context;
    ArrayList<MediaModel> mediaList;
    DeleteMediaListener listener;
    AppProgressDialog appProgressDialog;

    public DeleteAllMediaAsync(Context context, ArrayList<MediaModel> mediaList, DeleteMediaListener listener) {
        this.context = context;
        this.mediaList = mediaList;
        this.listener = listener;
        appProgressDialog = new AppProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (appProgressDialog != null) {
            appProgressDialog.showDialog();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(context, FILE_PERMISSIONS)) {
            try {
                if (mediaList != null && mediaList.size() > 0) {
                    for (int i = 0; i < mediaList.size(); i++) {
                        File f = new File(mediaList.get(i).getFilePath());
                        if (f != null && f.exists()) {
                            FileUtils.deleteQuietly(f);
                        }
                    }
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        if (appProgressDialog != null) {
            appProgressDialog.dismissDialog();
        }
        listener.onResult(success);
    }

    public interface DeleteMediaListener {
        void onResult(boolean success);
    }
}