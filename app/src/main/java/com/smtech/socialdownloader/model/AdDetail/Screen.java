package com.smtech.socialdownloader.model.AdDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Screen {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("screen_name")
    @Expose
    private String screenName;
    @SerializedName("ad_types")
    @Expose
    private List<String> adTypes = null;
    @SerializedName("third_ad_types")
    @Expose
    private List<String> thirdAdTypes = null;
    @SerializedName("ad_status")
    @Expose
    private Integer adStatus;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public List<String> getAdTypes() {
        return adTypes;
    }

    public void setAdTypes(List<String> adTypes) {
        this.adTypes = adTypes;
    }

    public Integer getAdStatus() {
        return adStatus;
    }

    public void setAdStatus(Integer adStatus) {
        this.adStatus = adStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<String> getThirdAdTypes() {
        return thirdAdTypes;
    }

    public void setThirdAdTypes(List<String> thirdAdTypes) {
        this.thirdAdTypes = thirdAdTypes;
    }
}
