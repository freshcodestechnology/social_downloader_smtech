package com.smtech.socialdownloader.model.AdDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdConfig {

    @SerializedName("ad_provider")
    @Expose
    private AdProvider adProvider;
    @SerializedName("app")
    @Expose
    private App app;
    @SerializedName("third_party")
    @Expose
    private ArrayList<ThirdPartyData> thirdPartyDataArrayList;

    public AdProvider getAdProvider() {
        return adProvider;
    }

    public void setAdProvider(AdProvider adProvider) {
        this.adProvider = adProvider;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public ArrayList<ThirdPartyData> getThirdPartyDataArrayList() {
        return thirdPartyDataArrayList;
    }

    public void setThirdPartyDataArrayList(ArrayList<ThirdPartyData> thirdPartyDataArrayList) {
        this.thirdPartyDataArrayList = thirdPartyDataArrayList;
    }

}

