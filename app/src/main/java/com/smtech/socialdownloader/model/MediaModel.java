package com.smtech.socialdownloader.model;

import java.io.Serializable;

public class MediaModel implements Serializable {
    String filePath;
    boolean selected;

    public MediaModel(String filePath) {
        this.filePath = filePath;
        this.selected = false;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}