package com.smtech.socialdownloader.model.AdDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdProvider {
    @SerializedName("ad_provider")
    @Expose
    private String adProvider;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("ad_provider_id")
    @Expose
    private Integer adProviderId;
    @SerializedName("google_app_id")
    @Expose
    private String googleAppId;
    @SerializedName("facebook_app_id")
    @Expose
    private String facebookAppId;
    @SerializedName("is_app_open")
    @Expose
    private Integer isAppOpen;
    @SerializedName("is_timer")
    @Expose
    private Integer isTimer;
    @SerializedName("timer_interval")
    @Expose
    private Integer timerInterval;
    @SerializedName("activity_count")
    @Expose
    private Integer activityCount;
    @SerializedName("admob_app_open")
    @Expose
    private String admobAppOpen;
    @SerializedName("admob_banner")
    @Expose
    private String admobBanner;
    @SerializedName("admob_interstitial")
    @Expose
    private String admobInterstitial;
    @SerializedName("admob_interstitial_video")
    @Expose
    private String admobInterstitialVideo;
    @SerializedName("admob_rewarded")
    @Expose
    private String admobRewarded;
    @SerializedName("admob_rewarded_interstitial")
    @Expose
    private String admobRewardedInterstitial;
    @SerializedName("admob_native_advanced")
    @Expose
    private String admobNativeAdvanced;
    @SerializedName("admob_native_advanced_video")
    @Expose
    private String admobNativeAdvancedVideo;
    @SerializedName("fb_banner")
    @Expose
    private String fbBanner;
    @SerializedName("fb_native")
    @Expose
    private String fbNative;
    @SerializedName("fb_interstitial")
    @Expose
    private String fbInterstitial;
    @SerializedName("fb_rewarded")
    @Expose
    private String fbRewarded;
    @SerializedName("ad_status")
    @Expose
    private Integer adStatus;
    @SerializedName("display_extra_activities")
    @Expose
    private Integer display_extra_activities;

    public String getAdProvider() {
        return adProvider;
    }

    public void setAdProvider(String adProvider) {
        this.adProvider = adProvider;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getAdProviderId() {
        return adProviderId;
    }

    public void setAdProviderId(Integer adProviderId) {
        this.adProviderId = adProviderId;
    }

    public String getGoogleAppId() {
        return googleAppId;
    }

    public void setGoogleAppId(String googleAppId) {
        this.googleAppId = googleAppId;
    }

    public String getFacebookAppId() {
        return facebookAppId;
    }

    public void setFacebookAppId(String facebookAppId) {
        this.facebookAppId = facebookAppId;
    }

    public Integer getIsAppOpen() {
        return isAppOpen;
    }

    public void setIsAppOpen(Integer isAppOpen) {
        this.isAppOpen = isAppOpen;
    }

    public Integer getIsTimer() {
        return isTimer;
    }

    public void setIsTimer(Integer isTimer) {
        this.isTimer = isTimer;
    }

    public String getAdmobAppOpen() {
        return admobAppOpen;
    }

    public void setAdmobAppOpen(String admobAppOpen) {
        this.admobAppOpen = admobAppOpen;
    }

    public String getAdmobBanner() {
        return admobBanner;
    }

    public void setAdmobBanner(String admobBanner) {
        this.admobBanner = admobBanner;
    }

    public String getAdmobInterstitial() {
        return admobInterstitial;
    }

    public void setAdmobInterstitial(String admobInterstitial) {
        this.admobInterstitial = admobInterstitial;
    }

    public String getAdmobInterstitialVideo() {
        return admobInterstitialVideo;
    }

    public void setAdmobInterstitialVideo(String admobInterstitialVideo) {
        this.admobInterstitialVideo = admobInterstitialVideo;
    }

    public String getAdmobRewarded() {
        return admobRewarded;
    }

    public void setAdmobRewarded(String admobRewarded) {
        this.admobRewarded = admobRewarded;
    }

    public String getAdmobRewardedInterstitial() {
        return admobRewardedInterstitial;
    }

    public void setAdmobRewardedInterstitial(String admobRewardedInterstitial) {
        this.admobRewardedInterstitial = admobRewardedInterstitial;
    }

    public String getAdmobNativeAdvanced() {
        return admobNativeAdvanced;
    }

    public void setAdmobNativeAdvanced(String admobNativeAdvanced) {
        this.admobNativeAdvanced = admobNativeAdvanced;
    }

    public String getAdmobNativeAdvancedVideo() {
        return admobNativeAdvancedVideo;
    }

    public void setAdmobNativeAdvancedVideo(String admobNativeAdvancedVideo) {
        this.admobNativeAdvancedVideo = admobNativeAdvancedVideo;
    }

    public String getFbBanner() {
        return fbBanner;
    }

    public void setFbBanner(String fbBanner) {
        this.fbBanner = fbBanner;
    }

    public String getFbNative() {
        return fbNative;
    }

    public void setFbNative(String fbNative) {
        this.fbNative = fbNative;
    }

    public String getFbInterstitial() {
        return fbInterstitial;
    }

    public void setFbInterstitial(String fbInterstitial) {
        this.fbInterstitial = fbInterstitial;
    }

    public String getFbRewarded() {
        return fbRewarded;
    }

    public void setFbRewarded(String fbRewarded) {
        this.fbRewarded = fbRewarded;
    }

    public Integer getAdStatus() {
        return adStatus;
    }

    public void setAdStatus(Integer adStatus) {
        this.adStatus = adStatus;
    }

    public Integer getTimerInterval() {
        return timerInterval;
    }

    public void setTimerInterval(Integer timerInterval) {
        this.timerInterval = timerInterval;
    }

    public Integer getActivityCount() {
        return activityCount;
    }

    public void setActivityCount(Integer activityCount) {
        this.activityCount = activityCount;
    }

    public Integer getDisplay_extra_activities() {
        return display_extra_activities;
    }

    public void setDisplay_extra_activities(Integer display_extra_activities) {
        this.display_extra_activities = display_extra_activities;
    }
}
