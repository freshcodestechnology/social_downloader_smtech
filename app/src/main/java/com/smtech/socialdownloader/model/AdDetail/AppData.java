package com.smtech.socialdownloader.model.AdDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("screens")
    @Expose
    private List<Screen> screens = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Screen> getScreens() {
        return screens;
    }

    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }

}
