package com.smtech.socialdownloader.model.AdDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdPartyData {
    @SerializedName("ad_type")
    @Expose
    private String adType;
    @SerializedName("ad_type_id")
    @Expose
    private String adTypeId;
    @SerializedName("url")
    @Expose
    private String redirectUrl;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("media_url")
    @Expose
    private String mediaUrl;

    @Override
    public String toString() {
        return "ThirdPartyData{" +
                "adType='" + adType + '\'' +
                ", adTypeId='" + adTypeId + '\'' +
                ", redirectUrl='" + redirectUrl + '\'' +
                ", status=" + status +
                ", mediaUrl='" + mediaUrl + '\'' +
                '}';
    }

    public String getAdType() {
        return adType;
    }

    public void setAdType(String adType) {
        this.adType = adType;
    }

    public String getAdTypeId() {
        return adTypeId;
    }

    public void setAdTypeId(String adTypeId) {
        this.adTypeId = adTypeId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }
}
