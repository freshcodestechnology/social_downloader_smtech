package com.smtech.socialdownloader.RetrofitUtils;

import com.smtech.socialdownloader.model.TiktokModel;
import com.smtech.socialdownloader.model.TwitterResponse;
import com.google.gson.JsonObject;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface APIServices {
    @GET
    Observable<JsonObject> callResult(@Url String str, @Header("Cookie") String str2, @Header("User-Agent") String str3);

    @FormUrlEncoded
    @POST
    Observable<TwitterResponse> callTwitter(@Url String str, @Field("id") String str2);

    @POST
    Observable<TiktokModel> getTiktokData(@Url String str, @Body HashMap<String, String> hashMap);
}
