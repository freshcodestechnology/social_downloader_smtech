package com.smtech.socialdownloader.RetrofitUtils;

import android.app.Activity;

import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.model.TiktokModel;
import com.smtech.socialdownloader.model.TwitterResponse;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class CommonClassForAPI {
    private static CommonClassForAPI CommonClassForAPI;
    private static Activity mActivity;

    public static CommonClassForAPI getInstance(Activity activity) {
        if (CommonClassForAPI == null) {
            CommonClassForAPI = new CommonClassForAPI();
        }
        mActivity = activity;
        return CommonClassForAPI;
    }

    public void callTwitterApi(final DisposableObserver disposableObserver, String str, String str2) {
        RestClient.getInstance(mActivity).getService().callTwitter(str, str2).subscribeOn(Schedulers.io()).observeOn(Schedulers.newThread()).subscribe(new Observer<TwitterResponse>() {
            public void onSubscribe(Disposable disposable) {
            }

            public void onNext(TwitterResponse twitterResponse) {
                disposableObserver.onNext(twitterResponse);
            }

            public void onError(Throwable th) {
                disposableObserver.onError(th);
            }

            @Override
            public void onComplete() {
                disposableObserver.onComplete();
            }
        });
    }

    public void callTiktokVideo(final DisposableObserver disposableObserver, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("link", str);
        RestClient.getInstance(mActivity).getService().getTiktokData(AppConstant.TikTokUrl, hashMap).subscribeOn(Schedulers.io()).observeOn(Schedulers.newThread()).subscribe(new Observer<TiktokModel>() {
            public void onSubscribe(Disposable disposable) {
            }

            public void onNext(TiktokModel tiktokModel) {
                disposableObserver.onNext(tiktokModel);
            }

            public void onError(Throwable th) {
                disposableObserver.onError(th);
            }

            @Override
            public void onComplete() {
                disposableObserver.onComplete();
            }
        });
    }

}
