package com.smtech.socialdownloader.RetrofitUtils;

public interface RetrofitHttpListener {
    void onResponse(String WSTag, String response);
}