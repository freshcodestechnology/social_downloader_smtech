package com.smtech.socialdownloader.RetrofitUtils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.view.AppProgressDialog;

import java.util.Map;

public class HttpRequest {
    Context context;
    int method;
    String reqBody;
    String wsTag;
    RetrofitHttpListener wsListener;
    Map<String, String> headers;
    AppProgressDialog appProgressDialog;
    public static int GET = 1;
    public static int POST = 2;
    RequestQueue mRequestQueue;

    public HttpRequest(Context context, int method, String reqBody, String wsTag, RetrofitHttpListener wsListener) {
        this.context = context;
        this.method = method;
        this.reqBody = reqBody;
        this.wsTag = wsTag;
        this.wsListener = wsListener;
        this.mRequestQueue = Volley.newRequestQueue(context);
    }

    public HttpRequest execute(String url) {
        try {
            if (URLUtil.isValidUrl(url)) {
             /*   appProgressDialog = new AppProgressDialog(context);
                appProgressDialog.showDialog();*/

                int reqMethod = Request.Method.GET;
                if (method == GET) {
                    reqMethod = Request.Method.GET;
                } else if (method == POST) {
                    reqMethod = Request.Method.POST;
                }

                StringRequest jsonReq = new StringRequest(reqMethod, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (appProgressDialog != null)
                            appProgressDialog.dismissDialog();

                        try {
                            if (context != null && AppMethod.isJSONValid(response)) {
                                wsListener.onResponse(wsTag, response);
                            } else {
                                AppMethod.showAlertWithOk(context, context.getString(R.string.app_name), "Invalid Response : " + response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (context != null)
                                AppMethod.showAlertWithOk(context, context.getString(R.string.app_name), AppConstant.SOMETHING_WRONG_TRY_AGAIN);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Log.e("LOGGG Error", "onErrorResponse: " + error.networkResponse.toString());

                        /*if (appProgressDialog != null)
                            appProgressDialog.dismissDialog();
*/
                        try {
                            String errorMessage = "";
                            if (error instanceof NetworkError) {
                                errorMessage = AppConstant.NO_INTERNET_CONNECTION;
                            } else if (error instanceof ServerError) {
                                errorMessage = AppConstant.SOMETHING_WRONG_TRY_AGAIN;
                            } else if (error instanceof AuthFailureError) {
                                errorMessage = "Authentication Error!";
                            } else if (error instanceof ParseError) {
                                errorMessage = AppConstant.SOMETHING_WRONG_TRY_AGAIN;
                            } else if (error instanceof NoConnectionError) {
                                errorMessage = "Connection can not be established, Please try again!";
                            } else if (error instanceof TimeoutError) {
                                errorMessage = AppConstant.TIME_OUT;
                            } else {
                                errorMessage = TextUtils.isEmpty(error.getMessage()) ? "Unknown Error" : error.getMessage();
                            }
                            if (context != null)
                                AppMethod.showAlertWithOk(context, context.getString(R.string.app_name), errorMessage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        if (reqBody != null && AppMethod.isJSONValid(reqBody)) {
                            return reqBody.getBytes();
                        }
                        return super.getBody();
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        if (headers != null && headers.size() > 0) {
                            return headers;
                        }
                        return super.getHeaders();
                    }
                };
                mRequestQueue.add(jsonReq);
            } else {
                Log.e("WS Error", wsTag + " : Invalid URL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}