package com.smtech.socialdownloader.RetrofitUtils;

import android.app.Activity;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static Activity mActivity;
    private static final RestClient restClient = new RestClient();
    private static Retrofit retrofit;

    public static RestClient getInstance(Activity activity) {
        mActivity = activity;
        return restClient;
    }

    public APIServices getService() {
        retrofit = new Retrofit.Builder().
                baseUrl("https://www.instagram.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return (APIServices) retrofit.create(APIServices.class);
    }
}
