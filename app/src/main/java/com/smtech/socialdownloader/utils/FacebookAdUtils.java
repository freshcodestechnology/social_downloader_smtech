package com.smtech.socialdownloader.utils;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FacebookAdUtils {
    //fb interstitial ad
    public static void showFbInterstitialAd(Activity activity,boolean isFromResult) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId = adConfig.getAdProvider().getFbInterstitial() == null ? activity.getString(R.string.fb_interstitial) : adConfig.getAdProvider().getFbInterstitial();

            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);
            if (mediaCount % count == 0) {
                final InterstitialAd interstitialAd = new InterstitialAd(activity, adId);
                interstitialAd.setAdListener(new InterstitialAdListener() {
                    @Override
                    public void onInterstitialDisplayed(Ad ad) {
                        Log.e("FacebookAd", "Interstitial ad displayed.");
                    }

                    @Override
                    public void onInterstitialDismissed(Ad ad) {
                        Log.e("FacebookAd", "Interstitial ad dismissed.");
                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.e("FacebookAd", "Interstitial ad failed to load: " + adError.getErrorMessage());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbInterstitial);
                        Toast.makeText(activity, "FB Interstitial ad loaded!", Toast.LENGTH_SHORT).show();
                        interstitialAd.show();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        Log.e("FacebookAd", "Interstitial ad clicked!");
                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbInterstitial);
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                        Log.e("FacebookAd", "Interstitial ad impression logged!");
                    }
                });
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        interstitialAd.loadAd();
                    }
                });
            }
        }
    }

    //fb banner ad
    public static void showFbBannerAd(Activity activity, LinearLayout adContainer) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId = adConfig.getAdProvider().getFbBanner() == null ? activity.getString(R.string.fb_banner) : adConfig.getAdProvider().getFbBanner();
            AdView adView = new AdView(activity, adId, AdSize.BANNER_HEIGHT_50);
            adView.setAdListener(new AdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    Log.e("FacebookAd", "Banner ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbBanner);
                    Toast.makeText(activity, "FB banner ad loaded!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAdClicked(Ad ad) {
                    Log.e("FacebookAd", "Banner ad clicked");
                    FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbBanner);
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    Log.e("FacebookAd", "Banner ad impression logged!");
                }
            });
            adContainer.addView(adView);
            adView.loadAd();
        }
    }

    //fb rewarded ad
    public static void showFbRewardedAd(Activity activity, boolean isFromResult) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);
            if (mediaCount % count == 0) {
                RewardedVideoAd rewardedVideoAd = new RewardedVideoAd(activity, activity.getString(R.string.fb_rewarded));
                rewardedVideoAd.setAdListener(new RewardedVideoAdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.e("FacebookAd", "Rewarded ad failed to load: " + adError.getErrorMessage());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        rewardedVideoAd.show();
                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbRewarded);
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        Log.e("FacebookAd", "Rewarded ad clicked!");
                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbRewarded);
                    }

                    @Override
                    public void onRewardedVideoCompleted() {
                        Log.e("FacebookAd", "Rewarded ad completed!");
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                        Log.e("FacebookAd", "Rewarded ad impression logged!");
                    }

                    @Override
                    public void onRewardedVideoClosed() {
                        Log.e("FacebookAd", "Rewarded ad closed!");
                    }
                });
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rewardedVideoAd.loadAd();
                    }
                });

            }
        }

    }

    //fb native ad
    public static void showFbNativeAd(Activity activity, LinearLayout adContainer) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId = adConfig.getAdProvider().getFbNative() == null ? activity.getString(R.string.fb_native) : adConfig.getAdProvider().getFbNative();

            NativeAd nativeAd = new NativeAd(activity, adId);

            NativeAdListener nativeAdListener = new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {
                    Log.e("FacebookAd", "Native ad media downloaded");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    Log.e("FacebookAd", "Native ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbNative);
                    if (nativeAd == null || nativeAd != ad) {
                        return;
                    }
                    // Inflate Native Ad into Container
                    inflateAd(activity, nativeAd, adContainer);
                }

                @Override
                public void onAdClicked(Ad ad) {
                    Log.e("FacebookAd", "Native ad clicked");
                    FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.fbNative);
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    Log.e("FacebookAd", "Native ad impression logged!");
                }
            };

            // Request an ad
            nativeAd.loadAd(
                    nativeAd.buildLoadAdConfig()
                            .withAdListener(nativeAdListener)
                            .build());
        }


    }

    public static void inflateAd(Activity activity, @NonNull NativeAd nativeAd, LinearLayout adContainer) {

        nativeAd.unregisterView();
        NativeAdLayout nativeAdLayout = new NativeAdLayout(activity);
        nativeAdLayout.addView(adContainer);

        LayoutInflater inflater = LayoutInflater.from(activity);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.fb_native_ad_layout, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(activity, nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView, nativeAdMedia, nativeAdIcon, clickableViews);
    }
}