package com.smtech.socialdownloader.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;

import java.util.Arrays;

public class AdmobAdUtils {

    public static Dialog ad_load_dialoog;


    public static void IntentInterstitialAd(Activity activity,Intent intent){

        if (AppUtils.isAdmobAvailable(activity)) {
            AdmobAdUtils.showAdmobInterstitialAdIntent(activity, true, true,intent);
        } else {
            FacebookAdUtils.showFbInterstitialAd(activity, true);
        }
    }

    //admob small banner ad
    public static void showAdmobBannerAd(Activity activity, LinearLayout adContainer) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
          String adId = adConfig.getAdProvider().getAdmobBanner() == null ? activity.getString(R.string.admob_banner) : adConfig.getAdProvider().getAdmobBanner();
         //  String adId = activity.getString(R.string.admob_banner);
            AdRequest adRequest = new AdRequest.Builder().build();
            AdView adView = new AdView(activity);
            adView.setAdUnitId(adId);
          // adView.setAdUnitId( activity.getString(R.string.admob_banner));
            adContainer.addView(adView);
            AdSize adSize = getAdSize(activity);
            // Step 4 - Set the adaptive ad size on the ad view.
            adView.setAdSize(adSize);

            // Step 5 - Start loading the ad in the background.
            adView.loadAd(adRequest);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobBanner);
                }

                @Override
                public void onAdFailedToLoad(LoadAdError adError) {
                    Log.e("AdmobAd", "Failed to load!" + " " + adError.getMessage());
                }

                @Override
                public void onAdOpened() {
                   Log.e("AdmobAd", "Banner ad opened!");
                }

                @Override
                public void onAdClicked() {
                    Log.e("AdmobAd", "Banner ad clicked!");
                    FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobBanner);
                }

                @Override
                public void onAdClosed() {
                    Log.e("AdmobAd", "Banner ad closed!");
                }
            });
        }
    }

    public static AdSize getAdSize(Activity activity) {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(activity, adWidth);
    }


    //admob native advanced / native advanced video ad
    public static void refreshAd(Activity activity, LinearLayout adContainer, boolean isVideoAd) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
           String adId;
            if (isVideoAd) {
                adId = adConfig.getAdProvider().getAdmobNativeAdvancedVideo() == null ? activity.getString(R.string.admob_native_advanced_video) : adConfig.getAdProvider().getAdmobNativeAdvancedVideo();
            } else {
                adId = adConfig.getAdProvider().getAdmobNativeAdvanced() == null ? activity.getString(R.string.admob_native_advanced) : adConfig.getAdProvider().getAdmobNativeAdvanced();
            }

            AdLoader.Builder builder = new AdLoader.Builder(activity, adId);

            builder.forNativeAd(
                    new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                            NativeAdView adView =
                                    (NativeAdView) LayoutInflater.from(activity)
                                            .inflate(R.layout.admob_ad_unified, null);
                            populateUnifiedNativeAdView(nativeAd, adView);
                            adContainer.removeAllViews();
                          adContainer.addView(adView);

                        }
                    });

            VideoOptions videoOptions =
                    new VideoOptions.Builder().setStartMuted(false).build();

            NativeAdOptions adOptions =
                    new NativeAdOptions.Builder().setVideoOptions(videoOptions).build();

            builder.withNativeAdOptions(adOptions);

            AdLoader adLoader =
                    builder
                            .withAdListener(
                                    new AdListener() {
                                        @Override
                                        public void onAdLoaded() {
                                            FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobNativeAdvanced);
                                        }

                                        @Override
                                        public void onAdFailedToLoad(LoadAdError adError) {
                                            Log.e("AdmobAd", "Failed to load!" + " " + adError.getMessage());
                                        }

                                        @Override
                                        public void onAdOpened() {
                                            Log.e("AdmobAd", "Banner ad opened!");
                                        }

                                        @Override
                                        public void onAdClicked() {
                                            Log.e("AdmobAd", "Banner ad clicked!");
                                            FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobNativeAdvanced);
                                        }

                                        @Override
                                        public void onAdClosed() {
                                            Log.e("AdmobAd", "Banner ad closed!");
                                        }
                                    })
                            .build();

            adLoader.loadAd(new AdRequest.Builder().build());
        }

    }

    public static void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));


        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
        VideoController vc = nativeAd.getMediaContent().getVideoController();

        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
    }

    //admob interstitialAd / interstitialAd video ad
    public static void showAdmobInterstitialAd(Activity activity, boolean isFromResult,boolean isVideoAd) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
          String adId;

            if (isVideoAd) {
                adId = adConfig.getAdProvider().getAdmobInterstitialVideo() == null ? activity.getString(R.string.admob_interstitial_video) : adConfig.getAdProvider().getAdmobInterstitialVideo();
            } else {
                adId = adConfig.getAdProvider().getAdmobInterstitial() == null ? activity.getString(R.string.admob_interstitial) : adConfig.getAdProvider().getAdmobInterstitial();
            }
            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);

            Log.e("LOGG mediaCount", "showAdmobInterstitialAdIntent: "+mediaCount );

            if (mediaCount % count == 0) {
                AdRequest adRequest = new AdRequest.Builder().build();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                      //  AdsLoadingDialog(activity);

                        InterstitialAd.load(
                                activity,
                                adId,
                                adRequest,
                                new InterstitialAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                        interstitialAd.show(activity);
                                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                        interstitialAd.setFullScreenContentCallback(
                                                new FullScreenContentCallback() {
                                                    @Override
                                                    public void onAdDismissedFullScreenContent() {
                                                        super.onAdDismissedFullScreenContent();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad dismissed!");
                                                        /*if (ad_load_dialoog.isShowing()) {
                                                            ad_load_dialoog.dismiss();
                                                        }*/

                                                    }

                                                    @Override
                                                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                                        super.onAdFailedToShowFullScreenContent(adError);
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Failed to load interstitial!" + " " + adError.getMessage());
                                                        /*if (ad_load_dialoog.isShowing()) {
                                                            ad_load_dialoog.dismiss();
                                                        }*/
                                                    }

                                                    @Override
                                                    public void onAdShowedFullScreenContent() {
                                                        super.onAdShowedFullScreenContent();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad showed!");

                                                    }

                                                    @Override
                                                    public void onAdClicked() {
                                                        super.onAdClicked();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad clicked!");

                                                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                                    }

                                                });
                                    }

                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        /*if (ad_load_dialoog.isShowing()) {
                                            ad_load_dialoog.dismiss();
                                        }*/
                                        Log.e("AdmobAd", "showAdmobInterstitialAd Failed to load interstitial!" + " " + loadAdError.getMessage());
                                    }
                                });
                    }
                });
            }
        }
    }

    //admob interstitialAd / interstitialAd video ad with intent veriable
    public static void showAdmobInterstitialAdIntent(Activity activity, boolean isFromResult, boolean isVideoAd, Intent intent) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            //String adId;
            String adId = activity.getString(R.string.admob_interstitial);
            if (isVideoAd) {
                adId = adConfig.getAdProvider().getAdmobInterstitialVideo() == null ? activity.getString(R.string.admob_interstitial_video) : adConfig.getAdProvider().getAdmobInterstitialVideo();
            } else {
                adId = adConfig.getAdProvider().getAdmobInterstitial() == null ? activity.getString(R.string.admob_interstitial) : adConfig.getAdProvider().getAdmobInterstitial();
            }
            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);

            Log.e("LOGG mediaCount", "showAdmobInterstitialAdIntent: "+mediaCount );

            if (mediaCount % count == 0) {
                AdRequest adRequest = new AdRequest.Builder().build();
                String finalAdId = adId;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.e("AdmobAd", "AdRequest RUNEER");
                      //  AdsLoadingDialog(activity);

                        InterstitialAd.load(
                                activity,
                                finalAdId,
                                adRequest,
                                new InterstitialAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                        interstitialAd.show(activity);
                                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                        interstitialAd.setFullScreenContentCallback(
                                                new FullScreenContentCallback() {
                                                    @Override
                                                    public void onAdDismissedFullScreenContent() {
                                                        super.onAdDismissedFullScreenContent();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad dismissed!");

                                                        /*if (ad_load_dialoog.isShowing()) {
                                                            ad_load_dialoog.dismiss();
                                                        }*/

                                                        activity.startActivity(intent);
                                                    }

                                                    @Override
                                                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                                        super.onAdFailedToShowFullScreenContent(adError);
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Failed to load interstitial!" + " " + adError.getMessage());
                                                        /*if (ad_load_dialoog.isShowing()) {
                                                            ad_load_dialoog.dismiss();
                                                        }*/
                                                        activity.startActivity(intent);

                                                    }

                                                    @Override
                                                    public void onAdShowedFullScreenContent() {
                                                        super.onAdShowedFullScreenContent();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad showed!");
                                                    }

                                                    @Override
                                                    public void onAdClicked() {
                                                        super.onAdClicked();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad clicked!");
                                                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                                    }

                                                });
                                    }

                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        Log.e("AdmobAd", "showAdmobInterstitialAd Failed to load interstitial!" + " " + loadAdError.getMessage());

                                        /*if (ad_load_dialoog.isShowing()) {
                                            ad_load_dialoog.dismiss();
                                        }*/
                                        activity.startActivity(intent);

                                    }
                                });
                    }
                });
            }else {
                activity.startActivity(intent);
            }
        }else {
            activity.startActivity(intent);
        }

    }

    //admob interstitialAd / interstitialAd video ad with intent Check ads


    //admob rewarded ad
    public static void showAdmobRewardedAd(Activity activity, boolean isFromResult) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId = adConfig.getAdProvider().getAdmobRewarded() == null ? activity.getString(R.string.admob_rewarded) : adConfig.getAdProvider().getAdmobRewarded();

            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);
            if (mediaCount % count == 0) {
                AdRequest adRequest = new AdRequest.Builder().build();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RewardedAd.load(
                                activity,
                                adId,
                                adRequest,
                                new RewardedAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                                        rewardedAd.show(activity, rewardItem -> {
                                            Log.e("AdmobAd", "onRewarded: " + rewardItem.getAmount());
                                        });
                                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobRewarded);
                                        rewardedAd.setFullScreenContentCallback(
                                                new FullScreenContentCallback() {
                                                    @Override
                                                    public void onAdDismissedFullScreenContent() {
                                                        super.onAdDismissedFullScreenContent();
                                                        Log.e("AdmobAd", "Rewarded ad dismissed!");
                                                    }

                                                    @Override
                                                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                                        super.onAdFailedToShowFullScreenContent(adError);
                                                        Log.e("AdmobAd", "Failed to load rewardedAd!" + " " + adError.getMessage());
                                                    }

                                                    @Override
                                                    public void onAdShowedFullScreenContent() {
                                                        super.onAdShowedFullScreenContent();
                                                        Log.e("AdmobAd", "Rewarded ad showed!");
                                                    }

                                                    @Override
                                                    public void onAdClicked() {
                                                        super.onAdClicked();
                                                        Log.e("AdmobAd", "Rewarded ad clicked!");
                                                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobRewarded);
                                                    }

                                                });
                                    }

                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        Log.e("AdmobAd", "Failed to load RewardedAd!" + " " + loadAdError.getMessage());
                                    }
                                });
                    }
                });
            }
        }

    }

    //admob rewarded interstitial ad
    public static void showAdmobRewardedInterstitialAd(Activity activity, boolean isFromResult) {
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId = adConfig.getAdProvider().getAdmobRewardedInterstitial() == null ? activity.getString(R.string.admob_rewarded_interstitial) : adConfig.getAdProvider().getAdmobRewardedInterstitial();


            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);
            if (mediaCount % count == 0) {
                AdRequest adRequest = new AdRequest.Builder().build();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RewardedInterstitialAd.load(
                                activity,
                                adId,
                                adRequest,
                                new RewardedInterstitialAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
                                        rewardedInterstitialAd.show(activity, rewardItem -> {
                                            Log.e("AdmobAd", "onRewarded: " + rewardItem.getAmount());
                                        });
                                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobRewardedInterstitial);
                                        rewardedInterstitialAd.setFullScreenContentCallback(
                                                new FullScreenContentCallback() {
                                                    @Override
                                                    public void onAdDismissedFullScreenContent() {
                                                        super.onAdDismissedFullScreenContent();
                                                        Log.e("AdmobAd", "Rewarded interstitial ad dismissed!");
                                                    }

                                                    @Override
                                                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                                        super.onAdFailedToShowFullScreenContent(adError);
                                                        Log.e("AdmobAd", "Failed to load Rewarded interstitial!" + " " + adError.getMessage());
                                                    }

                                                    @Override
                                                    public void onAdShowedFullScreenContent() {
                                                        super.onAdShowedFullScreenContent();
                                                        Log.e("AdmobAd", "Rewarded interstitial ad showed!");
                                                    }

                                                    @Override
                                                    public void onAdClicked() {
                                                        super.onAdClicked();
                                                        Log.e("AdmobAd", "Rewarded interstitial ad clicked!");
                                                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobRewardedInterstitial);
                                                    }

                                                });
                                    }

                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        Log.e("AdmobAd", "Failed to load Rewarded interstitial!" + " " + loadAdError.getMessage());
                                    }
                                });
                    }
                });
            }
        }
    }

    public static void AdsLoadingDialog(Activity activity) {

        ad_load_dialoog = new Dialog(activity);
        ad_load_dialoog.setContentView(R.layout.ad_load_dialog);
        ad_load_dialoog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ad_load_dialoog.setCancelable(false);
        if (!activity.isFinishing())
        {
            ad_load_dialoog.show();
        }
    }

    public static void showAdmobInterstitialAdIntentCheck(Activity activity, boolean isFromResult, boolean isVideoAd) {

        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
//        AdConfig adConfig = null;
        if (adConfig != null && adConfig.getAdProvider().getAdStatus() == 1) {
            String adId;
            if (isVideoAd) {
                adId = adConfig.getAdProvider().getAdmobInterstitialVideo() == null ? activity.getString(R.string.admob_interstitial_video) : adConfig.getAdProvider().getAdmobInterstitialVideo();
            } else {
                adId = adConfig.getAdProvider().getAdmobInterstitial() == null ? activity.getString(R.string.admob_interstitial) : adConfig.getAdProvider().getAdmobInterstitial();
            }
            int count = adConfig.getAdProvider().getActivityCount();
            int mediaCount = AppUtils.getMediaCount(activity, isFromResult,count);

            Log.e("LOGG mediaCount", "showAdmobInterstitialAdIntent: "+mediaCount );

            if (mediaCount % count == 0) {
                AdRequest adRequest = new AdRequest.Builder().build();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //  AdsLoadingDialog(activity);

                        InterstitialAd.load(
                                activity,
                                adId,
                                adRequest,
                                new InterstitialAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                        interstitialAd.show(activity);
                                        FirebaseAnalyticsUtils.adLoaded(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                        interstitialAd.setFullScreenContentCallback(
                                                new FullScreenContentCallback() {
                                                    @Override
                                                    public void onAdDismissedFullScreenContent() {
                                                        super.onAdDismissedFullScreenContent();
                                                        if (ad_load_dialoog.isShowing()) {
                                                            ad_load_dialoog.dismiss();
                                                        }
                                                    }

                                                    @Override
                                                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                                        super.onAdFailedToShowFullScreenContent(adError);

                                                        if (ad_load_dialoog.isShowing())
                                                        {
                                                            ad_load_dialoog.dismiss();
                                                        }

                                                    }

                                                    @Override
                                                    public void onAdShowedFullScreenContent() {
                                                        super.onAdShowedFullScreenContent();

                                                    }

                                                    @Override
                                                    public void onAdClicked() {
                                                        super.onAdClicked();
                                                        Log.e("AdmobAd", "showAdmobInterstitialAd Interstitial ad clicked!");
                                                        FirebaseAnalyticsUtils.adClicked(activity, Arrays.toString(activity.getClass().getSimpleName().split("Activity")), AppConstant.admobInterstitial);
                                                    }

                                                });
                                    }

                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        Log.e("AdmobAd", "showAdmobInterstitialAd Failed to load interstitial!" + " " + loadAdError.getMessage());

                                        /*if (ad_load_dialoog.isShowing())
                                        {
                                            ad_load_dialoog.dismiss();
                                        }*/

                                        ad_load_dialoog.dismiss();

                                    }
                                });
                    }
                });
            }
        }

    }

}