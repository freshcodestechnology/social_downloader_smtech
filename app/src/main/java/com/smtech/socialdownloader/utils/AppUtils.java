package com.smtech.socialdownloader.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.model.AdDetail.AdData;
import com.smtech.socialdownloader.model.AdDetail.Screen;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppUtils {

    /// after 09/02/2022

    public static AdConfig getAdConfigFromPreference(Activity activity) {
        Object objectJson = AppMethod.getPreferenceObjectJson(activity, AppConstant.PREF_AD_CONFIG, AdConfig.class);
        AdConfig adConfig = (AdConfig) objectJson;
        return adConfig;
    }

    public static AdData getAdDataFromPreference(Activity activity) {
        Object objectJson = AppMethod.getPreferenceObjectJson(activity, AppConstant.PREF_AD_DATA, AdData.class);
        AdData adData = (AdData) objectJson;
        return adData;
    }

    public static void getShowAdData(Activity activity, String tag, LinearLayout adContainer, LinearLayout adContainernative) {

        AdData adData = getAdDataFromPreference(activity);


        if (adData != null) {
            List<Screen> screenList = adData.getAppData().getScreens();
            for (Screen screen : screenList) {
                if (screen.getScreenName().matches(tag) && screen.getAdStatus() == 1) {
                    List<String> stringList = screen.getAdTypes();
                    if (isAdmobAvailable(activity)) {
                        if (adContainer != null && adContainernative != null) {
                            if (stringList.contains(AppConstant.admobNativeAdvanced)) {
                                AdmobAdUtils.refreshAd(activity, adContainernative, false);
                            }
                            if (stringList.contains(AppConstant.admobNativeAdvancedVideo)) {
                                AdmobAdUtils.refreshAd(activity, adContainernative, true);
                            }
                            if (stringList.contains(AppConstant.admobBanner)) {
                                AdmobAdUtils.showAdmobBannerAd(activity, adContainer);
                            }

                        }

                        else if (adContainer != null && adContainernative == null){
                            if (stringList.contains(AppConstant.admobBanner)) {
                                AdmobAdUtils.showAdmobBannerAd(activity, adContainer);
                            }

                        }
                        else if (adContainer == null && adContainernative != null){
                            if (stringList.contains(AppConstant.admobNativeAdvanced)) {
                                AdmobAdUtils.refreshAd(activity, adContainernative, false);
                            }


                        }

                        else {
                            if (stringList.contains(AppConstant.admobNativeAdvanced)) {
                                AdmobAdUtils.refreshAd(activity, adContainer, false);
                            }
                            if (stringList.contains(AppConstant.admobNativeAdvancedVideo)) {
                                AdmobAdUtils.refreshAd(activity, adContainer, true);
                            }
                            if (stringList.contains(AppConstant.admobBanner)) {
                                AdmobAdUtils.showAdmobBannerAd(activity, adContainer);
                            }

                        }
                    }


                    else {
                        if (stringList.contains(AppConstant.fbBanner)) {
                            FacebookAdUtils.showFbBannerAd(activity, adContainer);
                        }
                        if (stringList.contains(AppConstant.fbNative)) {
                            FacebookAdUtils.showFbNativeAd(activity, adContainer);
                        }
                        if (stringList.contains(AppConstant.fbInterstitial)) {
                            FacebookAdUtils.showFbInterstitialAd(activity, false);
                        }
                        if (stringList.contains(AppConstant.fbRewarded)) {
                            FacebookAdUtils.showFbRewardedAd(activity, false);
                        }
                    }

                }
            }

        }
    }

    public static void getShowThirdPartyAdData(Activity activity, String tag, LinearLayout thirdBannerContainer, LinearLayout thirdNativeContainer) {
        AdData adData = AppUtils.getAdDataFromPreference(activity);
        if (adData != null) {
            List<Screen> screenList = adData.getAppData().getScreens();
            for (Screen screen : screenList) {
                if (screen.getScreenName().matches(tag) && screen.getAdStatus() == 1) {
                    List<String> stringList = screen.getThirdAdTypes();
                    if (AppUtils.isAdmobAvailable(activity)) {
                        if (thirdBannerContainer != null) {
                            if (stringList.contains(AppConstant.thirdPartyBanner)) {
                                ThirdPartyAdUtils.showThirdPartyBannerAd(activity, thirdBannerContainer);
                            }
                        }
                    }
                }
            }
        }
    }

    /// before 09/02/2022
    public static String whatsAppDirectory = "Android/media/com.whatsapp/WhatsApp/Media/.Statuses";
    public static String waStatusDirectory = "/WhatsApp/Media/.Statuses";

    public static String getFacebookDirPath = "/Social Media Downloader/Facebook/";
    public static String getInstagramDirPath = "/Social Media Downloader/Instagram/";

    public static String RootDirectoryLikee = "/Social Media Downloader/Likee/";
    public static File RootDirectoryLikeeShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/Likee");

    public static String RootDirectoryRoposo = "/Social Media Downloader/Roposo/";
    public static File RootDirectoryRoposoShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/Roposo");

    public static String RootDirectoryShareChat = "/Social Media Downloader/ShareChat/";
    public static File RootDirectoryShareChatShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/ShareChat");

    public static String RootDirectoryTikTok = "/Social Media Downloader/TikTok/";
    public static File RootDirectoryTikTokShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/TikTok");

    public static String RootDirectoryTwitter = "/Social Media Downloader/Twitter/";
    public static File RootDirectoryTwitterShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/Twitter");

    public static String RootDirectoryWhatsApp = "/Social Media Downloader/WhatsApp/";
    public static File RootDirectoryWhatsAppShow = new File(Environment.getExternalStorageDirectory() + "/Download/Social Media Downloader/WhatsApp");

    public static void createFileFolder() {
        if (!RootDirectoryTikTokShow.exists()) {
            RootDirectoryTikTokShow.mkdirs();
        }
        if (!RootDirectoryTwitterShow.exists()) {
            RootDirectoryTwitterShow.mkdirs();
        }
        if (!RootDirectoryLikeeShow.exists()) {
            RootDirectoryLikeeShow.mkdirs();
        }
        if (!RootDirectoryLikeeShow.exists()) {
            RootDirectoryLikeeShow.mkdirs();
        }
        if (!RootDirectoryShareChatShow.exists()) {
            RootDirectoryShareChatShow.mkdirs();
        }
        if (!RootDirectoryRoposoShow.exists()) {
            RootDirectoryRoposoShow.mkdirs();
        }
        if (!RootDirectoryWhatsAppShow.exists()) {
            RootDirectoryWhatsAppShow.mkdirs();
        }
    }

    public static boolean saveWhatsappStatus(Context context, File file) {
        try {
            //  File f = new File(Environment.DIRECTORY_DOWNLOADS, AppUtils.RootDirectoryWhatsApp);
            String filename = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1);
            File destFile = new File(RootDirectoryWhatsAppShow.getPath() + File.separator + filename);
            if (!destFile.getParentFile().exists())
                destFile.getParentFile().mkdirs();
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            FileChannel source = null;
            FileChannel destination = null;

            try {
                source = new FileInputStream(file).getChannel();
                destination = new FileOutputStream(destFile).getChannel();
                destination.transferFrom(source, 0, source.size());
            } finally {
                if (source != null) {
                    source.close();
                }
                if (destination != null) {
                    destination.close();
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int getMediaCount(Activity activity, boolean isFromResult,int count) {
        int mediaCount;
        if (isFromResult) {
            int flagMediaCount = AppMethod.getIntegerPreference(activity, AppConstant.PREF_MEDIA_COUNT);
            AppMethod.setIntegerPreference(activity, AppConstant.PREF_MEDIA_COUNT, flagMediaCount + 1);
            mediaCount = AppMethod.getIntegerPreference(activity, AppConstant.PREF_MEDIA_COUNT);
        } else {
            mediaCount = count;
        }
        return mediaCount;
    }

    public static boolean isAdmobAvailable(Activity activity) {
        String adType = AppMethod.getStringPreference(activity, AppConstant.PREF_AD_TYPE);
        return adType.equalsIgnoreCase(AppConstant.admob);
    }

    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }

    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    public static ArrayList<String> getInstaDownloadURLsFromResponse(String response) {
        ArrayList<String> downloadUrls = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("graphql")) {
                JSONObject graphqObj = jsonObject.getJSONObject("graphql");
                if (graphqObj.has("shortcode_media")) {
                    JSONObject shortCodeMediaObj = graphqObj.getJSONObject("shortcode_media");
                    if (shortCodeMediaObj.has("edge_sidecar_to_children")) {
                        JSONObject edgesSidecarToChildrenObj = shortCodeMediaObj.getJSONObject("edge_sidecar_to_children");
                        if (edgesSidecarToChildrenObj.has("edges")) {
                            JSONArray edgesArray = edgesSidecarToChildrenObj.getJSONArray("edges");
                            if (edgesArray != null && edgesArray.length() > 0) {
                                for (int i = 0; i < edgesArray.length(); i++) {
                                    JSONObject edgeObj = edgesArray.getJSONObject(i);
                                    if (edgeObj.has("node")) {
                                        JSONObject nodeObj = edgeObj.getJSONObject("node");
                                        if (nodeObj.has("is_video")) {
                                            boolean isVideo = nodeObj.getBoolean("is_video");
                                            if (isVideo) {
                                                if (nodeObj.has("video_url")) {
                                                    downloadUrls.add(nodeObj.getString("video_url"));
                                                }
                                            } else {
                                                if (nodeObj.has("display_url")) {
                                                    downloadUrls.add(nodeObj.getString("display_url"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (shortCodeMediaObj.has("is_video")) {
                            boolean isVideo = shortCodeMediaObj.getBoolean("is_video");
                            if (isVideo) {
                                if (shortCodeMediaObj.has("video_url")) {
                                    downloadUrls.add(shortCodeMediaObj.getString("video_url"));
                                }
                            } else {
                                if (shortCodeMediaObj.has("display_url")) {
                                    downloadUrls.add(shortCodeMediaObj.getString("display_url"));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return downloadUrls;
    }

    @SuppressLint("WrongConstant")
    public static void startDownload(String str, String str2, Activity activity, String str3) {
        Toast.makeText(activity, "Downloading Started...", Toast.LENGTH_SHORT).show();
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
        request.setAllowedNetworkTypes(3);
        request.setNotificationVisibility(1);
        request.setTitle(str3 + "");
        request.setVisibleInDownloadsUi(true);
        String str4 = Environment.DIRECTORY_DOWNLOADS;
        request.setDestinationInExternalPublicDir(str4, str2 + str3);
        ((DownloadManager) activity.getSystemService("download")).enqueue(request);
        try {
            if (Build.VERSION.SDK_INT >= 19) {
                MediaScannerConnection.scanFile(activity, new String[]{new File(Environment.DIRECTORY_DOWNLOADS + "/" + str2 + str3).getAbsolutePath()}, (String[]) null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String str, Uri uri) {
                    }
                });
                return;
            }
            activity.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.fromFile(new File(Environment.DIRECTORY_DOWNLOADS + "/" + str2 + str3))));
            FirebaseAnalyticsUtils.statusDownload(activity, true, Arrays.toString(activity.getClass().getSimpleName().split("Activity")));
            if (AppUtils.isAdmobAvailable(activity)) {
                AdmobAdUtils.showAdmobInterstitialAd(activity, true, true);
            } else {
                FacebookAdUtils.showFbInterstitialAd(activity, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}