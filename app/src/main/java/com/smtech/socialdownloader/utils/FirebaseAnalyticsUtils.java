package com.smtech.socialdownloader.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.smtech.socialdownloader.globle.AppMethod;

public class FirebaseAnalyticsUtils {
    public static void statusDownload(Context context, boolean isVideo, String isFrom) {
        String deviceId = AppMethod.getUDID(context);
        try {
            @SuppressLint("MissingPermission")
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            mFirebaseAnalytics.setUserId(deviceId);
            Bundle parameters = new Bundle();
            if (isVideo) {
                parameters.putString("type", "Video");
            } else {
                parameters.putString("type", "Image");
            }
            parameters.putString("userId", deviceId);
            parameters.putString("isFrom", isFrom);
            mFirebaseAnalytics.logEvent("DownloadStatus", parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteStatus(Context context, String isFrom) {
        String deviceId = AppMethod.getUDID(context);
        try {
            @SuppressLint("MissingPermission")
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            mFirebaseAnalytics.setUserId(deviceId);
            Bundle parameters = new Bundle();
            parameters.putString("userId", deviceId);
            parameters.putString("isFrom", isFrom);
            mFirebaseAnalytics.logEvent("DeleteStatus", parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void adClicked(Context context, String isFrom, String adType) {
        String deviceId = AppMethod.getUDID(context);
        try {
            @SuppressLint("MissingPermission")
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            mFirebaseAnalytics.setUserId(deviceId);
            Bundle parameters = new Bundle();
            parameters.putString("userId", deviceId);
            parameters.putString("isFrom", isFrom);
            parameters.putString("adType", adType);
            mFirebaseAnalytics.logEvent("AdClicked", parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void adLoaded(Context context, String isFrom, String adType) {
        String deviceId = AppMethod.getUDID(context);
        try {
            @SuppressLint("MissingPermission")
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            mFirebaseAnalytics.setUserId(deviceId);
            Bundle parameters = new Bundle();
            parameters.putString("userId", deviceId);
            parameters.putString("isFrom", isFrom);
            parameters.putString("adType", adType);
            mFirebaseAnalytics.logEvent("AdLoaded", parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}