package com.smtech.socialdownloader.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.model.AdDetail.ThirdPartyConstants;
import com.smtech.socialdownloader.model.AdDetail.ThirdPartyData;

import java.util.ArrayList;
import java.util.Random;

public class ThirdPartyAdUtils {
    public static void showThirdPartyBannerAd(Activity activity, LinearLayout thirdBannerContainer){
        AdConfig adConfig = AppUtils.getAdConfigFromPreference(activity);
        if (adConfig != null) {
         //   Log.d("thirdPArtyBanner", adConfig.getThirdPartyDataArrayList().size()+"");
            ArrayList<ThirdPartyData> thirdPartyDataList = new ArrayList<ThirdPartyData>();
            for(ThirdPartyData tempData : adConfig.getThirdPartyDataArrayList()){
                if(tempData.getAdType().equals(AppConstant.thirdPartyBanner)){
                    thirdPartyDataList.add(tempData);
                  //  Log.d("thirdPArtyBanner", tempData.getMediaUrl());
                }
            }
            final int randomIndex = new Random().nextInt(thirdPartyDataList.size());
            ImageView adBannerImage =  getImage(activity,thirdPartyDataList.get(randomIndex));
            thirdBannerContainer.addView(adBannerImage);
        }
    }

    static ImageView getImage(Activity activity, ThirdPartyData thirdPartyData) {
        ImageView imgView = new ImageView(activity);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgView.setLayoutParams(lp);
        Glide.with(activity)
                .load(Uri.parse(thirdPartyData.getMediaUrl()))
                .apply(new RequestOptions().placeholder(R.drawable.banner).error(R.drawable.banner))
                .into(imgView);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(thirdPartyData.getRedirectUrl());
                Intent gotoThirdParty = new Intent(Intent.ACTION_VIEW, uri);
                gotoThirdParty.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    activity.startActivity(gotoThirdParty);
                } catch (ActivityNotFoundException e) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(ThirdPartyConstants.redirectUrl)));
                }
            }
        });
        return imgView;
    }

    static ImageView getGif(Activity activity, String url) {
        ImageView imgView = new ImageView(activity);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgView.setLayoutParams(lp);
        Glide.with(activity)
                .load("https://ads.freshcodes.in/public/uploads/ad_media/1645002950.gif")
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(imgView);
        return imgView;
    }
}
