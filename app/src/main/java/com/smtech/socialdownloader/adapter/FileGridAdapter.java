package com.smtech.socialdownloader.adapter;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.async.VideoDurationAsync;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.MediaModel;

import java.io.File;
import java.util.ArrayList;

public class FileGridAdapter extends RecyclerView.Adapter<FileGridAdapter.ViewHolder> {
    Context context;
    ArrayList<MediaModel> mediaList;
    LayoutInflater inflater;

    public FileGridAdapter(Context context, ArrayList<MediaModel> mediaList) {
        this.context = context;
        this.mediaList = mediaList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(inflater.inflate(R.layout.adapter_file_grid, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        try {
            File file = new File(mediaList.get(i).getFilePath());
            if (file != null && file.exists()) {
                final String uri = Uri.fromFile(file).toString();
                final String decoded = Uri.decode(uri);
                RequestOptions requestOptions=new RequestOptions();
                requestOptions.placeholder(R.drawable.icon_place_holder);
                Glide.with(context).load(decoded).apply(requestOptions).into(viewHolder.rvPreviewImage);
                if (AppMethod.getMimeType(file.getAbsolutePath()).toLowerCase().contains("video")) {
                    viewHolder.llVideoInfo.setVisibility(View.GONE);
                    new VideoDurationAsync(context, file, new VideoDurationAsync.VideoDurationAsyncListener() {
                        @Override
                        public void onResult(boolean success, long timeInMillisec) {
                            if (success) {
                                viewHolder.txtDuration.setText(convertSecondsToHMmSs(timeInMillisec / 1000));
                                viewHolder.llVideoInfo.setVisibility(View.VISIBLE);
                            } else {
                                viewHolder.llVideoInfo.setVisibility(View.GONE);
                            }
                        }
                    }).execute();
                } else {
                    viewHolder.llVideoInfo.setVisibility(View.GONE);
                }
                if (mediaList.get(i).isSelected()) {
                    viewHolder.llSelected.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.llSelected.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView rvPreviewImage;
        LinearLayout llVideoInfo;
        TextView txtDuration;

        LinearLayout llSelected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rvPreviewImage = (ImageView) itemView.findViewById(R.id.rvPreviewImage);
            llVideoInfo = (LinearLayout) itemView.findViewById(R.id.llVideoInfo);
            txtDuration = (TextView) itemView.findViewById(R.id.txtDuration);

            llSelected = (LinearLayout) itemView.findViewById(R.id.llSelected);
        }
    }

    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        if (h > 0) {
            return String.format("%d:%02d:%02d", h, m, s);
        }
        return String.format("%02d:%02d", m, s);
    }
}