package com.smtech.socialdownloader.adapter;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.imageloader.DisplayImageOption;
import com.smtech.socialdownloader.view.OnSingleClickListener;

import java.io.File;
import java.net.URLConnection;
import java.util.ArrayList;

public class SavedPreviewAdapter extends PagerAdapter {
    Context context;
    ArrayList<File> statusFileList;
    SavedOptionListener savedOptionListener;
    LayoutInflater inflater;

    public SavedPreviewAdapter(Context context, ArrayList<File> statusFileList, SavedOptionListener savedOptionListener) {
        this.context = context;
        this.statusFileList = statusFileList;
        this.savedOptionListener = savedOptionListener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return statusFileList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view;
        view = inflater.inflate(R.layout.adapter_saved_preview, container, false);

        ImageView imgPreviewImage = (ImageView) view.findViewById(R.id.imgPreviewImage);
        LinearLayout llOptions = (LinearLayout) view.findViewById(R.id.llOptions);
        LinearLayout llShare = (LinearLayout) view.findViewById(R.id.llShare);
        LinearLayout llPlayVideo = (LinearLayout) view.findViewById(R.id.llPlayVideo);
        LinearLayout llDelete = (LinearLayout) view.findViewById(R.id.llDelete);

        final String uri = Uri.fromFile(statusFileList.get(position)).toString();
        final String decoded = Uri.decode(uri);
        ImageLoader.getInstance().displayImage(decoded, imgPreviewImage, DisplayImageOption.getDisplayImage());

        if (isVideoFile(statusFileList.get(position).getAbsolutePath())) {
            llPlayVideo.setVisibility(View.VISIBLE);
        } else {
            llPlayVideo.setVisibility(View.GONE);
        }

        llShare.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                savedOptionListener.onShareClick(position);
            }
        });

        llPlayVideo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                savedOptionListener.onVideoPlayClick(position);
            }
        });

        llDelete.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                savedOptionListener.onDeleteClick(position);
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    public interface SavedOptionListener {
        void onShareClick(int position);

        void onVideoPlayClick(int position);

        void onDeleteClick(int position);
    }
}