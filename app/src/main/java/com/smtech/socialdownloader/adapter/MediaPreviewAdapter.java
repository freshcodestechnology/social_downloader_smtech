package com.smtech.socialdownloader.adapter;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.imageloader.DisplayImageOption;

import java.io.File;
import java.util.ArrayList;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerDispatcher;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.helper.ToroPlayerHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;
import im.ene.toro.widget.PressablePlayerSelector;

public class MediaPreviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<String> filePathList;
    PressablePlayerSelector selector;
    MediaPreviewListener listener;
    LayoutInflater inflater;

    boolean fromWAStatus = false;

    private static final int TYPE_IMAGE = 1;
    private static final int TYPE_VIDEO = 2;

    public MediaPreviewAdapter(Context context, ArrayList<String> filePathList, PressablePlayerSelector selector, MediaPreviewListener listener) {
        this.context = context;
        this.filePathList = filePathList;
        this.selector = selector;
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imgPreviewImage;

        LinearLayout llOptions;
        LinearLayout llShare, llDownload, llDelete;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPreviewImage = (AppCompatImageView) itemView.findViewById(R.id.imgPreviewImage);

            llOptions = (LinearLayout) itemView.findViewById(R.id.llOptions);
            llShare = (LinearLayout) itemView.findViewById(R.id.llShare);
            llDownload = (LinearLayout) itemView.findViewById(R.id.llDownload);
            llDelete = (LinearLayout) itemView.findViewById(R.id.llDelete);

            llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.shareClick(getAdapterPosition());
                }
            });
            llDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.downloadClick(getAdapterPosition());
                }
            });
            llDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.deleteClick(getAdapterPosition());
                }
            });

            if (fromWAStatus) {
                llDownload.setVisibility(View.VISIBLE);
                llDelete.setVisibility(View.GONE);
            } else {
                llDownload.setVisibility(View.GONE);
                llDelete.setVisibility(View.VISIBLE);
            }
        }
    }

    class VideoViewHolder extends RecyclerView.ViewHolder implements ToroPlayer {
        ToroPlayerHelper helper;
        private Uri mediaUri;

        PlayerView player;

        LinearLayout llOptions;
        LinearLayout llShare, llDownload, llDelete;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            player = (PlayerView) itemView.findViewById(R.id.player);
            if (selector != null)
                player.setControlDispatcher(new ExoPlayerDispatcher(selector, this));

            llOptions = (LinearLayout) itemView.findViewById(R.id.llOptions);
            llShare = (LinearLayout) itemView.findViewById(R.id.llShare);
            llDownload = (LinearLayout) itemView.findViewById(R.id.llDownload);
            llDelete = (LinearLayout) itemView.findViewById(R.id.llDelete);

            llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.shareClick(getAdapterPosition());
                }
            });
            llDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.downloadClick(getAdapterPosition());
                }
            });
            llDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.deleteClick(getAdapterPosition());
                }
            });

            if (fromWAStatus) {
                llDownload.setVisibility(View.VISIBLE);
                llDelete.setVisibility(View.GONE);
            } else {
                llDownload.setVisibility(View.GONE);
                llDelete.setVisibility(View.VISIBLE);
            }
        }

        void bind(Uri mediaUri) {
            this.mediaUri = mediaUri;
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return player;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            if (helper == null) {
                helper = new ExoPlayerViewHelper(this, mediaUri);
            }
            helper.initialize(container, playbackInfo);
        }

        @Override
        public void play() {
            if (helper != null) helper.play();
        }

        @Override
        public void pause() {
            if (helper != null) helper.pause();
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.isPlaying();
        }

        @Override
        public void release() {
            if (helper != null) {
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == TYPE_IMAGE) {
            return new ImageViewHolder(inflater.inflate(R.layout.adapter_media_image, viewGroup, false));
        } else if (i == TYPE_VIDEO) {
            return new VideoViewHolder(inflater.inflate(R.layout.adapter_media_video, viewGroup, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        try {
            File file = new File(filePathList.get(i));
            if (file != null && file.exists()) {
                if (viewHolder.getItemViewType() == TYPE_IMAGE) {
                    ImageViewHolder holder = (ImageViewHolder) viewHolder;
                    final String uri = Uri.fromFile(file).toString();
                    final String decoded = Uri.decode(uri);
                    ImageLoader.getInstance().displayImage(decoded, holder.imgPreviewImage, DisplayImageOption.getDisplayImage());
                } else if (viewHolder.getItemViewType() == TYPE_VIDEO) {
                    VideoViewHolder holder = (VideoViewHolder) viewHolder;
                    holder.bind(Uri.fromFile(file));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return filePathList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (AppMethod.getMimeType(filePathList.get(position)).toLowerCase().contains("image")) {
            return TYPE_IMAGE;
        } else if (AppMethod.getMimeType(filePathList.get(position)).toLowerCase().contains("video")) {
            return TYPE_VIDEO;
        }
        return super.getItemViewType(position);
    }

    public boolean isFromWAStatus() {
        return fromWAStatus;
    }

    public void setFromWAStatus(boolean fromWAStatus) {
        this.fromWAStatus = fromWAStatus;
    }

    public interface MediaPreviewListener {
        void shareClick(int position);

        void downloadClick(int position);

        void deleteClick(int position);
    }
}