package com.smtech.socialdownloader;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefData {

    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    Context _context;

    public static String PolicyST = "PolicyST";

    // Shared pref mode
    public static int PRIVATE_MODE = 0;
    public static String PREF_NAME = "preference_data";

    public PrefData(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
}
