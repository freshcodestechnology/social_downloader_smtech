package com.smtech.socialdownloader.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.google.android.material.tabs.TabLayout;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.adapter.FileGridAdapter;
import com.smtech.socialdownloader.async.DeleteAllMediaAsync;
import com.smtech.socialdownloader.async.GetAllFilesAsync;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.MediaModel;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FacebookAdUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;
import com.smtech.socialdownloader.utils.ItemClickSupport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

public class GalleryActivity extends Activity {
    TabLayout tabType;
    LinearLayout llNoData;
    RecyclerView rvFiles;
    ArrayList<MediaModel> savedMediaList;
    FileGridAdapter mediaGridAdapter;
    LinearLayout llBottomOption;
    LinearLayout llShare, llDownload, llDelete;
    boolean showOptions = false;
    int selectedItemCnt = 0;
    private ImageView iconHelp;
    private ImageView iconBack;
    private TextView txtTitle;
    private static final int FILE_PERMISSION_REQUEST_CODE = 7841;
    private static final int REQUEST_CODE_SAVED_PREVIEW = 4654;
    LinearLayout adContainer;
    private static final String TAG = "GalleryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(GalleryActivity.this);
        if (isConnectedNetwork(GalleryActivity.this)) {
            MyApplication.activity = this;
            init();
            setUpEvents();
            AppUtils.getShowAdData(GalleryActivity.this, TAG, adContainer, null);
        } else {
            NoInternetConnection(GalleryActivity.this);
        }






    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        tabType = (TabLayout) findViewById(R.id.tabType);
       // tabType.addTab(tabType.newTab().setText("Whatsapp"));
        tabType.addTab(tabType.newTab().setText("Facebook"));
        tabType.addTab(tabType.newTab().setText("Instagram"));
        tabType.addTab(tabType.newTab().setText("Likee"));
      //  tabType.addTab(tabType.newTab().setText("Twitter"));
        tabType.addTab(tabType.newTab().setText("Share chat"));
        tabType.addTab(tabType.newTab().setText("TikTok"));
        tabType.addTab(tabType.newTab().setText("Roposo"));

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);

        llNoData = (LinearLayout) findViewById(R.id.llNoData);
        llNoData.setVisibility(View.GONE);

        rvFiles = (RecyclerView) findViewById(R.id.rvFiles);
        rvFiles.setLayoutManager(new GridLayoutManager(GalleryActivity.this, 3));
        savedMediaList = new ArrayList<>();
        mediaGridAdapter = new FileGridAdapter(GalleryActivity.this, savedMediaList);
        rvFiles.setAdapter(mediaGridAdapter);

        llBottomOption = (LinearLayout) findViewById(R.id.llBottomOption);
        llBottomOption.setVisibility(View.GONE);
        llShare = (LinearLayout) findViewById(R.id.llShare);
        llDownload = (LinearLayout) findViewById(R.id.llDownload);
        llDownload.setVisibility(View.GONE);
        llDelete = (LinearLayout) findViewById(R.id.llDelete);
        iconHelp.setVisibility(View.GONE);
    }

    private void setUpEvents() {
        txtTitle.setText("Gallery");
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getData(0);
        tabType.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getData(tabType.getSelectedTabPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ItemClickSupport.addTo(rvFiles).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (showOptions) {
                    savedMediaList.get(position).setSelected(!savedMediaList.get(position).isSelected());
                    mediaGridAdapter.notifyItemChanged(position);
                    if (savedMediaList.get(position).isSelected()) {
                        selectedItemCnt++;
                    } else {
                        selectedItemCnt--;
                    }

                    if (selectedItemCnt == 0) {
                        showOptions = false;
                        if (llBottomOption.getVisibility() == View.VISIBLE) {
                            llBottomOption.setVisibility(View.GONE);
                        }
                    }
                } else {
                    ArrayList<String> filePathList = new ArrayList<>();
                    for (int i = 0; i < savedMediaList.size(); i++) {
                        filePathList.add(savedMediaList.get(i).getFilePath());
                    }
                    AdmobAdUtils.showAdmobInterstitialAdIntentCheck(GalleryActivity.this, true, true);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            if (AdmobAdUtils.ad_load_dialoog != null) {
                                if (AdmobAdUtils.ad_load_dialoog.isShowing()) {
                                    handler.postDelayed(this, 100);
                                } else {
                                    Intent intent = new Intent(GalleryActivity.this, MediaPreviewActivity.class);
                                    intent.putExtra("filePathList", filePathList);
                                    intent.putExtra("initPos", position);
                                    intent.putExtra("fromWAStatus", false);
                                    startActivityForResult(intent, REQUEST_CODE_SAVED_PREVIEW);
                                }
                            } else {
                                Intent intent = new Intent(GalleryActivity.this, MediaPreviewActivity.class);
                                intent.putExtra("filePathList", filePathList);
                                intent.putExtra("initPos", position);
                                intent.putExtra("fromWAStatus", false);
                                startActivityForResult(intent, REQUEST_CODE_SAVED_PREVIEW);
                            }
                        }
                    }, 100);
                }
            }
        });

        ItemClickSupport.addTo(rvFiles).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, final int position, View v) {
                if (!showOptions) {
                    showOptions = true;
                    selectedItemCnt = 1;
                    savedMediaList.get(position).setSelected(true);
                    mediaGridAdapter.notifyItemChanged(position);
                    if (llBottomOption.getVisibility() == View.GONE) {
                        llBottomOption.setVisibility(View.VISIBLE);
                    }
                    return true;
                }
                return false;
            }
        });
        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intent.setType("*/*");

                    ArrayList<Uri> files = new ArrayList<Uri>();
                    for (int i = 0; i < savedMediaList.size(); i++) {
                        if (savedMediaList.get(i).isSelected()) {
                            File file = new File(savedMediaList.get(i).getFilePath());
                            Uri uri = Uri.fromFile(file);
                            files.add(uri);
                        }
                    }
                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] APP_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (AppMethod.hasPermissions(GalleryActivity.this, APP_PERMISSIONS)) {
                    AppMethod.showInfoDialog(GalleryActivity.this, getString(R.string.app_name), "Are you sure you want to delete this media?", true, "Yes", "No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ArrayList<MediaModel> list = new ArrayList<>();
                            for (int i = 0; i < savedMediaList.size(); i++) {
                                if (savedMediaList.get(i).isSelected()) {
                                    list.add(savedMediaList.get(i));
                                }
                            }
                            if (list != null && list.size() > 0) {
                                new DeleteAllMediaAsync(GalleryActivity.this, list, new DeleteAllMediaAsync.DeleteMediaListener() {
                                    @Override
                                    public void onResult(boolean success) {
                                        if (success) {
                                            Toast.makeText(GalleryActivity.this, "Media Deleted Successfully!", Toast.LENGTH_SHORT).show();
                                            getData(tabType.getSelectedTabPosition());
                                            FirebaseAnalyticsUtils.deleteStatus(GalleryActivity.this, "Gallery");
                                            if (AppUtils.isAdmobAvailable(GalleryActivity.this)) {
                                                AdmobAdUtils.showAdmobInterstitialAd(GalleryActivity.this, true, true);
                                            } else {
                                                FacebookAdUtils.showFbInterstitialAd(GalleryActivity.this, true);
                                            }
                                        } else {
                                            Toast.makeText(GalleryActivity.this, AppConstant.SOMETHING_WRONG_TRY_AGAIN, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).execute();
                            }
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else {
                    Toast.makeText(GalleryActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @SuppressLint("NewApi")
    private void getData(int position) {
        String directoryPath = "";
        switch (position) {
            case 0:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.getFacebookDirPath;
                break;
            case 1:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.getInstagramDirPath;
                break;
            case 2:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.RootDirectoryLikee;
                break;
           /* case 4:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.RootDirectoryTwitter;
                break;*/
            case 4:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.RootDirectoryShareChat;
                break;
            case 5:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.RootDirectoryTikTok;
                break;
            case 6:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.RootDirectoryRoposo;
                break;
            default:
                directoryPath = Environment.DIRECTORY_DOWNLOADS + AppUtils.getFacebookDirPath;
                break;
        }
        showOptions = false;
        selectedItemCnt = 0;
        llBottomOption.setVisibility(View.GONE);
        String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(GalleryActivity.this, FILE_PERMISSIONS)) {
            new GetAllFilesAsync(GalleryActivity.this, directoryPath, new GetAllFilesAsync.EventListener() {
                @Override
                public void onResult(ArrayList<MediaModel> mediaList) {
                    if (mediaList != null && mediaList.size() > 0) {
                        savedMediaList.clear();
                        savedMediaList.addAll(mediaList);
                        mediaGridAdapter.notifyDataSetChanged();
                        llNoData.setVisibility(View.GONE);
                    } else {
                        llNoData.setVisibility(View.VISIBLE);
                    }
                }
            }).execute();
        } else {
            requestPermissions(FILE_PERMISSIONS, FILE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SAVED_PREVIEW && resultCode == Activity.RESULT_OK) {
            getData(tabType.getSelectedTabPosition());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case FILE_PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    getData(tabType.getSelectedTabPosition());
                } else {
                    llNoData.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
}