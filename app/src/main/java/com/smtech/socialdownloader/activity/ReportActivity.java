package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.PrefData.PolicyST;
import static com.smtech.socialdownloader.PrefData.editor;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.ad_load_dialoog;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.showAdmobInterstitialAdIntentCheck;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;

public class ReportActivity extends AppCompatActivity {

    private final String TAG = "ReportActivity";

    private LinearLayout AgreeLinearLayout;
    private CheckBox termCheckbox;
    private TextView termTextview;
    private TextView PolicyTextView;
    private Intent intent;
    int reportstaus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(ReportActivity.this);
        reportstaus = getIntent().getIntExtra("statusforreport",0);
        Log.i(TAG, "reporstatus: "+reportstaus);

        if (isConnectedNetwork(ReportActivity.this)) {

            PolicyTextView = (TextView) findViewById(R.id.PolicyTextView);
            termCheckbox = (CheckBox) findViewById(R.id.termCheckbox);
            termTextview = (TextView) findViewById(R.id.termTextview);
            AgreeLinearLayout = (LinearLayout) findViewById(R.id.AgreeLinearLayout);

            PolicyTextView.setText("This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.\n" +
                    "\n" +
                    "If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Report.\n" +
                    "\n" +
                    "The terms used in this Report have the same meanings as in our Terms and Conditions, which are accessible at all apps unless otherwise defined in this Report.\n" +
                    "\n" +
                    "I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third-party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.");

            termCheckbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    termCheckbox.setChecked(true);
                }
            });

            termTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    termCheckbox.setChecked(true);
                }
            });

            AgreeLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (termCheckbox.isChecked()) {

                        editor.putInt(PolicyST, 1);
                        editor.commit();

                        showAdmobInterstitialAdIntentCheck(ReportActivity.this,true,true);
                        Handler handler=new Handler();
                        handler.postDelayed(new Runnable(){
                            public void run(){
                                if (ad_load_dialoog!=null) {
                                    if (ad_load_dialoog.isShowing()) {
                                        handler.postDelayed(this, 100);

                                    } else {
                                        moveToSecondary();
                                    }
                                }else {
                                    moveToSecondary();
                                }
                            }
                        }, 100);

                    } else {

                        Toast.makeText(ReportActivity.this, "Accept Term & Condition", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {

            NoInternetConnection(ReportActivity.this);
        }
    }

    public void moveToSecondary(){
        Intent intent;
        if(reportstaus==1){

            intent = new Intent(this,LetsStartActivity.class);
        }
        else {
           intent = new Intent(this,HomeActivity.class);
        }

        startActivity(intent);
    }
}