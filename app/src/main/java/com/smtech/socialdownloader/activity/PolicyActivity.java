package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.ad_load_dialoog;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.showAdmobInterstitialAdIntentCheck;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;

public class PolicyActivity extends AppCompatActivity {

    private final String TAG = "PolicyActivity";
    private LinearLayout AgreeLinearLayout;
    private CheckBox termCheckbox;
    private TextView termTextview;
    private TextView PolicyTextView;
    private Intent intent;
    String prevStarted = "yes";
   int status;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(PolicyActivity.this);
        status = getIntent().getIntExtra("status",0);



        if (isConnectedNetwork(PolicyActivity.this)) {

            PolicyTextView = (TextView) findViewById(R.id.PolicyTextView);
            termCheckbox = (CheckBox) findViewById(R.id.termCheckbox);
            termTextview = (TextView) findViewById(R.id.termTextview);
            AgreeLinearLayout = (LinearLayout) findViewById(R.id.AgreeLinearLayout);

            PolicyTextView.setText("The Privacy Policy applies for app Social Media Downloader in Android.\n" +
                    "\n" +
                    "User Entered Data\n" +
                    "All data entered in this app by user stores on user's device. The developer of this app has no means to access the data in any way. You are responsible for backing up the data in sdcard.\n" +
                    "\n" +
                    "Internet and File Write Permission\n" +
                    "This app needs internet access to retrieve Ad ,tracking events & Push Notification. File Write Permission is used for the storing & fetch media from external storage.\n" +
                    "\n" +
                    "Ads\n" +
                    "We may display ads supplied by a third party ad provider, such as Google AdMob, AdSense, Facebook Ads, etc. These providers may be using cookies, or other technologies to collect information as a result of ad serving.\n" +
                    "\n" +
                    "Important notes:\n" +
                    "This application is NOT affiliated or endorsed by Facebook, Instgram & WhatsApp.\n" +
                    "\n" +
                    "Any unauthorized reloading or downloading of content and / or violations of intellectual property. The rights are the sole responsibility of the user." +
                    "Information We Collect\n" +
                    "\n" +
                    "\n" +
                    "Personal Information Personal Information is the type of information that specifically identifies or contacts a person, such as your full name or email address. We may collect personally identification information whenever Users enter into our app, fill out the form, to use our services, features or resources present in our app. In the following circumstances, we may disclose your personal information according to your wish or regulations by law: 1) Your prior permission, 2) By the applicable law within or outside your country of residence, legal process, litigation requests,`1 3) By requests from public and governmental authorities, 4) To protect our legal rights and interests.\n" +
                    "\n" +
                    "\n" +
                    "Non-Personal Information Non-personal information is data in a form that does not permit direct association with any specific individual, such as your install, uninstall, Android ID, phone IMEI number, phone model etc. We may collect and use non-personal information in the following circumstances.\n" +
                    "\n" +
                    "\n" +
                    "1) To have a better understanding of the user’s behavior.\n" +
                    "\n" +
                    "\n" +
                    "2) Solve problems in products and services.\n" +
                    "\n" +
                    "\n" +
                    "3) Improve our products, services, and advertising.\n" +
                    "\n" +
                    "\n" +
                    "We may collect non-personal information such as the data of install, frequency of use, country, and channel.\n" +
                    "\n" +
                    "\n" +
                    "Information we get by using our services We may collect information about the services that you use and how you use them, such as when you view and interact with our content. We may collect device-specific information (such as your hardware model, os version, and unique device identifiers). Wots App's App will not share that information with third parties.\n" +
                    "\n" +
                    "\n" +
                    "Location information When you use a location-enabled Film Studio's App service, we may collect and process information about your actual location through GPS signals sent by a mobile device or Wifi in order to obtain your location for the purposes of providing our Service. We may also use various technologies to determine location, such as sensor data from your device that may, for example, provide information on nearby Wi-Fi access points and cell towers.\n" +
                    "\n" +
                    "\n" +
                    "Unique application numbers Certain services include a unique application number. This number and information about your installation (for example, the operating system type) may be sent to Wots App's App when you install or uninstall that service or when that service periodically contacts our servers, such as for automatic updates." +
                    "We take permission like\n" +
                    "\n" +
                    " android.permission.INTERNET,\n" +
                    "\n" +
                    "android.permission.ACCESS_NETWORK_STATE, android.permission.READ_EXTERNAL_STORAGE, android.permission.WRITE_EXTERNAL_STORAGE,\n" +
                    "\n" +
                    "android.permission.CAMERA,\n" +
                    "\n" +
                    "android.permission.SET_WALLPAPER,\n" +
                    "\n" +
                    "android.permission.MANAGE_EXTERNAL_STORAGE,\n" +
                    "\n" +
                    "we use this permission for save image and video on user device but do not share any information." +
                    "Kids Privacy Policy\n" +
                    "\n" +
                    "\n" +
                    "This Kids Privacy Policy has to be specified to indicate you, with guidance regarding our privacy policies with respect to collecting, using and disclosing personal information, regarding the legal guardian of a child under the age of 13 years old. Many of our Games/Apps are intended for general audiences, and we do not knowingly gather or use any Personal Information from children and kids under the age of 13. When users are identified as under 13, we will block such users from providing Personal Information or make sure to get prior parental consent before collecting Personal Information. If you are a parent of a child under 13 years of age and you think your child has provided us with Personal Information, please contact us. We offers mobile applications and games (Kids Apps), which are targeted to children under the age of 13 and also other apps are not targeted under the age of 13. If you have additional questions about our Privacy Practices related to children under the age of 13, please contact us." +
                    "Safeguards\n" +
                    "\n" +
                    "\n" +
                    "We store Personal information collected by us in secure operating environments that are not obtainable to the public and that are only accessible by authorized employees. We should also provide security measures in place to protect the loss, misuse, and alteration of the information under our control.");

            termCheckbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    termCheckbox.setChecked(true);
                }
            });

            termTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    termCheckbox.setChecked(true);
                }
            });

            AgreeLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (termCheckbox.isChecked()) {

                        showAdmobInterstitialAdIntentCheck(PolicyActivity.this,true,true);
                        Handler handler=new Handler();
                        handler.postDelayed(new Runnable(){
                            public void run(){
                                if (ad_load_dialoog!=null) {
                                    if (ad_load_dialoog.isShowing()) {
                                        handler.postDelayed(this, 100);

                                    } else {
                                        intent = new Intent(PolicyActivity.this, ReportActivity.class);
                                        intent.putExtra("statusforreport",status);
                                        startActivity(intent);
                                        finish();
                                    }
                                }else {
                                    intent = new Intent(PolicyActivity.this, ReportActivity.class);
                                    intent.putExtra("statusforreport",status);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }, 100);


                    } else {

                        Toast.makeText(PolicyActivity.this, "Accept Term & Condition", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {

            NoInternetConnection(PolicyActivity.this);
        }
    }
    public void moveToSecondary(){
        // use an intent to travel from one activity to another.
        Intent intent = new Intent(this,LetsStartActivity.class);
        startActivity(intent);
    }
}