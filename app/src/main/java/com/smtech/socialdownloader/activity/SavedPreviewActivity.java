package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;

import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.adapter.SavedPreviewAdapter;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.utils.AppUtils;

import java.io.File;
import java.util.ArrayList;

public class SavedPreviewActivity extends Activity implements SavedPreviewAdapter.SavedOptionListener {
    ArrayList<File> savedFileList;
    int position;
    Toolbar mToolbar;

    ViewPager pagerSavedPreview;
    SavedPreviewAdapter savedPreviewAdapter;

    private static final int PERMISSION_REQUEST_CODE = 1001;
    private static final String TAG = "SavedPreviewActivity";
    boolean updateFlag = false;
    LinearLayout adContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_preview);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(SavedPreviewActivity.this);
        if (isConnectedNetwork(SavedPreviewActivity.this)) {
            MyApplication.activity = this;
            initArguments();
            init();
            AppUtils.getShowAdData(SavedPreviewActivity.this, TAG, adContainer, null);
        } else {
            NoInternetConnection(SavedPreviewActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

    private void initArguments() {
        if (getIntent().hasExtra("savedFileList")) {
            savedFileList = (ArrayList<File>) getIntent().getSerializableExtra("savedFileList");
            position = getIntent().getIntExtra("position", 0);
        } else {
            SavedPreviewActivity.this.finish();
        }
    }

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        pagerSavedPreview = (ViewPager) findViewById(R.id.pagerSavedPreview);
        savedPreviewAdapter = new SavedPreviewAdapter(SavedPreviewActivity.this, savedFileList, this);
        pagerSavedPreview.setAdapter(savedPreviewAdapter);
        pagerSavedPreview.setCurrentItem(position);
    }

    @Override
    public void onShareClick(int position) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri screenshotUri = Uri.fromFile(savedFileList.get(position));
        sharingIntent.setType("*/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

    @Override
    public void onVideoPlayClick(int position) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromFile(savedFileList.get(position)));
        intent.setDataAndType(Uri.fromFile(savedFileList.get(position)), "video/*");
        startActivity(intent);
    }

    @Override
    public void onDeleteClick(final int position) {
        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(SavedPreviewActivity.this, PERMISSIONS)) {
            AppMethod.showInfoDialog(SavedPreviewActivity.this, getString(R.string.app_name), "Are you sure you want to delete this file?", true, "Yes", "No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (savedFileList.get(position).exists()) {
                        if (savedFileList.get(position).delete()) {
                            updateFlag = true;
                            savedFileList.remove(position);
                            savedPreviewAdapter.notifyDataSetChanged();
                            if (savedFileList.size() == 0) {
                                Intent resultIntent = new Intent();
                                setResult(RESULT_OK, resultIntent);
                                SavedPreviewActivity.this.finish();
                            }
                        } else {
                            AppMethod.showAlertWithOk(SavedPreviewActivity.this, getString(R.string.app_name), "Error occured during delete this file!");
                        }
                    }
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            ActivityCompat.requestPermissions(SavedPreviewActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    AppMethod.showInfoDialog(SavedPreviewActivity.this, getString(R.string.app_name), "Are you sure you want to delete this file?", true, "Yes", "No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (savedFileList.get(position).exists()) {
                                if (savedFileList.get(position).delete()) {
                                    updateFlag = true;
                                    savedFileList.remove(position);
                                    savedPreviewAdapter.notifyDataSetChanged();
                                    if (savedFileList.size() == 0) {
                                        Intent resultIntent = new Intent();
                                        setResult(RESULT_OK, resultIntent);
                                        SavedPreviewActivity.this.finish();
                                    }
                                } else {
                                    AppMethod.showAlertWithOk(SavedPreviewActivity.this, getString(R.string.app_name), "Error occured during delete this file!");
                                }
                            }
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (updateFlag) {
            Intent resultIntent = new Intent();
            setResult(RESULT_OK, resultIntent);
            SavedPreviewActivity.this.finish();
        } else
            super.onBackPressed();
    }
}