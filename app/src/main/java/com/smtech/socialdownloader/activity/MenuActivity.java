package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;

public class MenuActivity extends AppCompatActivity {

    private final String TAG = "MenuActivity";
    private LinearLayout WADirectMsgLinearLayout,VideoDownloadLinearLayout;
    private Intent intent;
    private LinearLayout PlayGameLinearLayout;
    private LinearLayout native_container;
    private LinearLayout banner_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(MenuActivity.this);

        if (isConnectedNetwork(MenuActivity.this)) {

            banner_container = (LinearLayout)findViewById(R.id.banner_container);
          //  native_container = (LinearLayout)findViewById(R.id.native_container);
            AppUtils.getShowAdData(MenuActivity.this, TAG, banner_container, null);

            WADirectMsgLinearLayout = (LinearLayout) findViewById(R.id.WADirectMsgLinearLayout);
            VideoDownloadLinearLayout = (LinearLayout) findViewById(R.id.VideoDownloadLinearLayout);
            PlayGameLinearLayout = (LinearLayout) findViewById(R.id.PlayGameLinearLayout);

            VideoDownloadLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    intent = new Intent(MenuActivity.this, HomeActivity.class);
                    AdmobAdUtils.IntentInterstitialAd(MenuActivity.this,intent);
                }
            });

            WADirectMsgLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    intent = new Intent(MenuActivity.this, WADirectActivity.class);
                    AdmobAdUtils.IntentInterstitialAd(MenuActivity.this,intent);
                }
            });

            PlayGameLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }else {

            NoInternetConnection(MenuActivity.this);
        }
    }
}