package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.Constant.NoInternetConnection;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.RetrofitUtils.CommonClassForAPI;
import com.smtech.socialdownloader.model.TiktokModel;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;
import com.smtech.socialdownloader.view.OnSingleClickListener;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import io.reactivex.observers.DisposableObserver;

public class TikTokActivity extends Activity {

    private EditText etUrl;
    private Button btnPaste,btnOKinsta;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2,iconInsta;
    private TextView txtTitle, txtAppTitle;
    CommonClassForAPI commonClassForAPI;
    public String VideoUrl;
    AppProgressDialog appProgressDialog;
    boolean isShowTutorial = false;
    private CardView cardTutorial;
    LinearLayout adContainer, adContainernative;
    LinearLayout thirdBannerContainer;
    private static final String TAG = "TikTokActivity";

    ImageView imgTutorial_1,imgTutorial_2;
    Prefs prefs;
    ScrollView tiktokscroll;


    private final DisposableObserver<TiktokModel> tiktokObserver = new DisposableObserver<TiktokModel>() {
        public void onNext(TiktokModel tiktokModel) {
            appProgressDialog.showDialog();
            try {
                if (!tiktokModel.getResponsecode().equals("200")) {
                    return;
                }
                AppUtils.startDownload(tiktokModel.getData().getMainvideo(), AppUtils.RootDirectoryTikTok, TikTokActivity.this, getFilenameFromURL(tiktokModel.getData().getMainvideo()));
                etUrl.setText("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onError(Throwable th) {
            appProgressDialog.dismissDialog();
            appProgressDialog.showDialog();
            new callGetTikTokData().execute(etUrl.getText().toString());
        }

        public void onComplete() {
            appProgressDialog.dismissDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(TikTokActivity.this);
        if (isConnectedNetwork(TikTokActivity.this)) {
            MyApplication.activity = this;
            prefs = new Prefs(this);
            init();
            setUpEvents();
            if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                adContainernative.setVisibility(View.GONE);
                thirdBannerContainer.setVisibility(View.GONE);
            }
            else {
                AppUtils.getShowAdData(TikTokActivity.this, TAG, adContainer, adContainernative);
                AppUtils.getShowThirdPartyAdData(TikTokActivity.this, TAG, thirdBannerContainer, null);
            }

        } else {
            NoInternetConnection(TikTokActivity.this);
        }

        tiktokscroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = tiktokscroll.getScrollY(); // For ScrollView
                int scrollX = tiktokscroll.getScrollX(); // For HorizontalScrollView
                if(scrollX<scrollY){
                    adContainer.setVisibility(View.VISIBLE);
                }
                else  {
                    adContainer.setVisibility(View.GONE);
                }
            }
        });
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }*/

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
    //    thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container_insta);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        txtTitle = (TextView) findViewById(R.id.txtTitle_fb);
        // txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        //  appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);
        iconInsta = (ImageView) findViewById(R.id.iconInsta);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        imgTutorial_1 = (ImageView) findViewById(R.id.imgTutorial_1);
        imgTutorial_2 = (ImageView) findViewById(R.id.imgTutorial_2);
        btnOKinsta = (Button) findViewById(R.id.btnOKinsta_button);
        tiktokscroll =  findViewById(R.id.fb_scroll);

        appProgressDialog = new AppProgressDialog(TikTokActivity.this);
        commonClassForAPI = CommonClassForAPI.getInstance(TikTokActivity.this);
        AppUtils.createFileFolder();
    }

    private void setUpEvents() {
        txtTitle.setText("TikTok");
        //  txtAppTitle.setText("TikTok");
        //  appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_tiktok));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_tiktok_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_tiktok_2));

        imgTutorial_1.setImageDrawable(getResources().getDrawable(R.drawable.img_tiktok_1));
        imgTutorial_2.setImageDrawable(getResources().getDrawable(R.drawable.img_tiktok_2));

        iconInsta.setImageDrawable(getResources().getDrawable(R.drawable.tiktok_icon));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnOKinsta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cardTutorial.setVisibility(View.GONE);
                    }
                });

                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }

            }

        });

        etUrl.setHint("Enter or Paste TikTok URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(TikTokActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String obj = etUrl.getText().toString();
                if (TextUtils.isEmpty(obj)) {
                    etUrl.setError("Enter TikTok URL");
                } else {
                    GetTikTokData();
                }
            }
        });
    }

    public String getFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath()).getName() + ".mp4";
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }

    class callGetTikTokData extends AsyncTask<String, Void, Document> {
        Document tikDoc;

        callGetTikTokData() {
        }

        public void onPreExecute() {
            super.onPreExecute();
        }

        public Document doInBackground(String... strArr) {
            try {
                this.tikDoc = Jsoup.connect(strArr[0]).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.tikDoc;
        }

        public void onPostExecute(Document document) {
            appProgressDialog.dismissDialog();
            try {
                String html = document.select("script[id=\"__NEXT_DATA__\"]").last().html();
                if (!html.equals("")) {
                    try {
                        VideoUrl = String.valueOf(new JSONObject(html).getJSONObject("props").getJSONObject("pageProps").getJSONObject("videoData").getJSONObject("itemInfos").getJSONObject("video").getJSONArray("urls").get(0));

                        String access$100 = TikTokActivity.this.VideoUrl;
                        String str = AppUtils.RootDirectoryTikTok;
                        AppUtils.startDownload(access$100, str, TikTokActivity.this, "tiktok_" + System.currentTimeMillis() + ".mp4");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void GetTikTokData() {
        String obj = etUrl.getText().toString();
        try {
            AppUtils.createFileFolder();
            if (new URL(obj).getHost().contains("tiktok")) {
                appProgressDialog.showDialog();
                callVideoDownload(obj);
            } else {
                etUrl.setError("Invalid Tiktok URL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callVideoDownload(String str) {
        try {
            if (commonClassForAPI != null) {
                commonClassForAPI.callTiktokVideo(this.tiktokObserver, str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}