package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.IntentInterstitialAd;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.utils.AppUtils;

import java.io.ByteArrayOutputStream;

public class LetsStartActivity extends AppCompatActivity {

    private final String TAG = "LetsStartActivity";
    private LinearLayout LetsStartLinearLayout;
    private Intent intent;
    private LinearLayout shareLL;
    private LinearLayout rateLL;
    private LinearLayout policyLL;
    private LinearLayout banner_container;
    private LinearLayout native_container;
    int status;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lets_start);
        status=getIntent().getIntExtra("status",1);
        Log.i(TAG, "onCreate: "+status);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new PrefData(LetsStartActivity.this);
        if (isConnectedNetwork(LetsStartActivity.this)) {

            banner_container = (LinearLayout)findViewById(R.id.banner_container);
           // native_container = (LinearLayout)findViewById(R.id.native_container);
            AppUtils.getShowAdData(LetsStartActivity.this, TAG, banner_container, null);

            LetsStartLinearLayout = (LinearLayout) findViewById(R.id.LetsStartLinearLayout);
            shareLL = (LinearLayout) findViewById(R.id.shareLL);
            rateLL = (LinearLayout) findViewById(R.id.rateLL);
            policyLL = (LinearLayout) findViewById(R.id.policyLL);

            LetsStartLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    intent = new Intent(LetsStartActivity.this, MenuActivity.class);
                    IntentInterstitialAd(LetsStartActivity.this,intent);
                }
            });

            shareLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        if (isReadStorageAllowed()) {

                            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.banner);
                            Intent s = new Intent(Intent.ACTION_SEND);
                            s.setType("image/*");
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            b.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                            String path = MediaStore.Images.Media.insertImage(LetsStartActivity.this.getContentResolver(),
                                    b, "Title", null);
                            Uri imageUri = Uri.parse(path);

                            s.putExtra(Intent.EXTRA_TEXT, "*Social Media Downloader*"
                                    + "\n\n http://play.google.com/store/apps/details?id=" + LetsStartActivity.this.getPackageName()
                                    + "\n\n 1. Download All Status and image from all Social media"
                                    + "\n 2. Easily Download all videos"
                                    + "\n 3. Download Insta Story and post Images"
                                    + "\n 4. Easily Share with friends"
                                    + "\n\n http://play.google.com/store/apps/details?id=" + LetsStartActivity.this.getPackageName()
                                    + "\n\nPlease give the *5 Star \uD83C\uDF1F\uD83C\uDF1F\uD83C\uDF1F\uD83C\uDF1F\uD83C\uDF1F Rating* with a *longer review \uD83D\uDD8B️\uD83D\uDDD3️* on the Play Store."
                            );
                            s.putExtra(Intent.EXTRA_STREAM, imageUri);
                            startActivity(Intent.createChooser(s, "Share App"));

                        } else {

                            requestStoragePermission();
                        }

                    } catch (Exception e) {
                    }

                }
            });

            rateLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Dialog RateDialog = new Dialog(LetsStartActivity.this);
                    RateDialog.setContentView(R.layout.dialog_app_rating);
                    TextView rateButton = RateDialog.findViewById(R.id.btn_rt);
                    TextView laterButton = RateDialog.findViewById(R.id.btn_later);
                    RateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    final RatingBar ratingbar = (RatingBar) RateDialog.findViewById(R.id.ratingbar);

                    rateButton.setOnClickListener(view -> {

                        float rateValue = ratingbar.getRating();

                        if (rateValue > 0) {

                            if (rateValue < 4) {

                                Toast.makeText(LetsStartActivity.this, "Thanks for Giving Review", Toast.LENGTH_SHORT).show();

                                RateDialog.dismiss();

                            } else {

                                RateDialog.dismiss();

                                Uri uri = Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                // To count with Play market backstack, After pressing back button,
                                // to taken back to our application, we need to add following flags to intent.
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    LetsStartActivity.this.startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    LetsStartActivity.this.startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                                }

                            }

                        } else {

                            Toast.makeText(LetsStartActivity.this, "Please Give the Rating", Toast.LENGTH_SHORT).show();
                        }
                    });

                    laterButton.setOnClickListener(view -> RateDialog.dismiss());

                    RateDialog.show();
                }
            });

            policyLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        Uri uri = Uri.parse(getResources().getString(R.string.privacy_policy_link)); // missing 'http://' will cause crashed
                        Intent intent_policy = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent_policy);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(LetsStartActivity.this, "Please give a valid privacy policy URL.", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {

            NoInternetConnection(LetsStartActivity.this);
        }
    }

    //We are calling this method to check the permission status
    final String[] permision = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.RECORD_AUDIO"};

    private boolean isReadStorageAllowed() {
        //Getting the permission status
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(LetsStartActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0 &&
                    ContextCompat.checkSelfPermission(LetsStartActivity.this, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
                return true;
            } else {
                requestPermissions(this.permision, 100);
            }
        } else {
            return true;
        }
        //If permission is not granted returning false
        Toast.makeText(this, "Please Allow All Permission to Continue App.", Toast.LENGTH_SHORT).show();
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(LetsStartActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 23);
    }

    @Override
    public void onBackPressed()
    {
        intent = new Intent(LetsStartActivity.this, StartActivity.class);
        startActivity(intent);
        finish();
       /* if(status!=1)
        {
            intent = new Intent(LetsStartActivity.this, ExitAppActivity.class);
            startActivity(intent);
        }
        else {
            intent = new Intent(LetsStartActivity.this, StartActivity.class);
            startActivity(intent);
            finish();
        }*/

    }


}