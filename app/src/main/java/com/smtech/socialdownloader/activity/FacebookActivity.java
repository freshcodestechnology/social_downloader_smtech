package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FacebookAdUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;
import com.smtech.socialdownloader.view.OnSingleClickListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FacebookActivity extends Activity {
    private static final String TAG = "FacebookActivity";
    private EditText etUrl;
    private Button btnPaste,btnOKinsta;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2,iconInsta;
    private TextView txtTitle, txtAppTitle;
    private static final int FILE_PERMISSION_REQUEST_CODE = 4665;
    public String VideoUrl;
    boolean isShowTutorial = false;
    private CardView cardTutorial,cardTutorial_1;
    LinearLayout adContainer, adContainernative;
    LinearLayout thirdBannerContainer;

    ImageView imgTutorial_1,imgTutorial_2;
    Prefs prefs;
    ScrollView fb_scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(FacebookActivity.this);
        if (isConnectedNetwork(FacebookActivity.this)) {
            MyApplication.activity = this;
            prefs = new Prefs(this);
            init();
            setUpEvents();
            if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                adContainernative.setVisibility(View.GONE);
            }
            else {
                AppUtils.getShowAdData(FacebookActivity.this, TAG, adContainer, adContainernative);
            }
            AppUtils.getShowThirdPartyAdData(FacebookActivity.this, TAG, thirdBannerContainer, null);
        } else {

            NoInternetConnection(FacebookActivity.this);
        }
    }



    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
     //   thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container_insta);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        iconInsta = (ImageView) findViewById(R.id.iconInsta);
        txtTitle = (TextView) findViewById(R.id.txtTitle_fb);
        fb_scroll = findViewById(R.id.fb_scroll);
        //  txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        //appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);
        imgTutorial_1 = (ImageView) findViewById(R.id.imgTutorial_1);
        imgTutorial_2 = (ImageView) findViewById(R.id.imgTutorial_2);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        cardTutorial_1 = (CardView) findViewById(R.id.cardTutorial_1);
        btnOKinsta = (Button) findViewById(R.id.btnOKinsta_button);
    }

    private void setUpEvents() {
        //appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_fb));
        txtTitle.setText("Facebook");
        // txtAppTitle.setText("Facebook");
        imgTutorial_1.setImageDrawable(getResources().getDrawable(R.drawable.img_fb_1));
        imgTutorial_2.setImageDrawable(getResources().getDrawable(R.drawable.img_fb_2));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_fb_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_fb_2));
        iconInsta.setImageDrawable(getResources().getDrawable(R.drawable.fb_icon));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnOKinsta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cardTutorial.setVisibility(View.GONE);
                    }
                });

                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }

            }

        });

        etUrl.setHint("Enter or Paste Facebook URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(FacebookActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String fbVideoUrl = etUrl.getText().toString();
                if (TextUtils.isEmpty(fbVideoUrl)) {
                    etUrl.setError("Enter Facebook Video URL");
                } else {
                    try {
                        if (new URL(fbVideoUrl).getHost().contains("facebook") || new URL(fbVideoUrl).getHost().contains("fb")) {
                            new callGetFacebookData().execute(fbVideoUrl);
                        } else {
                            etUrl.setError("Invalid Facebook URL");
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        fb_scroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = fb_scroll.getScrollY(); // For ScrollView
                int scrollX = fb_scroll.getScrollX(); // For HorizontalScrollView
                if(scrollX<scrollY){
                    adContainer.setVisibility(View.VISIBLE);
                }
                else  {
                    adContainer.setVisibility(View.GONE);
                }
            }
        });
    }

    class callGetFacebookData extends AsyncTask<String, Void, Document> {
        Document facebookDoc;
        AppProgressDialog appProgressDialog;

        callGetFacebookData() {
        }

        public void onPreExecute() {
            super.onPreExecute();
            appProgressDialog = new AppProgressDialog(FacebookActivity.this);
            appProgressDialog.showDialog();
        }

        public Document doInBackground(String... strArr) {
            try {
                this.facebookDoc = Jsoup.connect(strArr[0]).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.facebookDoc;
        }

        public void onPostExecute(Document document) {
            try {
                VideoUrl = document.select("meta[property=\"og:video\"]").last().attr(FirebaseAnalytics.Param.CONTENT);
                if (!VideoUrl.equals("")) {
                    if (appProgressDialog != null)
                        appProgressDialog.dismissDialog();
                    try {

                        downloadFbVideo(VideoUrl);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (NullPointerException e2) {
                if (appProgressDialog != null)
                    appProgressDialog.dismissDialog();

                e2.printStackTrace();
            }
        }
    }

    public String getFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath()).getName() + ".mp4";
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }

    @SuppressLint("NewApi")
    private void downloadFbVideo(String fbMediaData) {
        String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(FacebookActivity.this, FILE_PERMISSIONS)) {
            Uri uri = Uri.parse(fbMediaData);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle(getString(R.string.app_name));

            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, AppUtils.getFacebookDirPath + getFilenameFromURL(fbMediaData));

            DownloadManager manager = (DownloadManager) FacebookActivity.this.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
            Toast.makeText(FacebookActivity.this, "Downloading Started...", Toast.LENGTH_SHORT).show();
            etUrl.setText("");
            FirebaseAnalyticsUtils.statusDownload(FacebookActivity.this, AppUtils.isVideoFile(new File(fbMediaData).getAbsolutePath()), "Facebook");

            if (AppUtils.isAdmobAvailable(FacebookActivity.this)) {
                AdmobAdUtils.showAdmobInterstitialAd(FacebookActivity.this, true, true);
            } else {
                FacebookAdUtils.showFbInterstitialAd(FacebookActivity.this, true);
            }
        } else {
            requestPermissions(FILE_PERMISSIONS, FILE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case FILE_PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    Toast.makeText(FacebookActivity.this, "Try now to view/download media!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}