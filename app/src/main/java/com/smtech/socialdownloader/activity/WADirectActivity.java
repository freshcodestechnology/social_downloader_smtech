package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.hbb20.CountryCodePicker;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.utils.AppUtils;

public class WADirectActivity extends Activity {

    CountryCodePicker countryPicker;
    EditText etMobileNo;
    Button btnOpenChat;
    private ImageView iconHelp;
    private ImageView iconBack,iconInsta;
    private TextView txtTitle;
    LinearLayout adContainer,native_container_insta;
    LinearLayout thirdBannerContainer;
    private static final String TAG = "WADirectActivity";
    Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wa_direct);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(WADirectActivity.this);
        if (isConnectedNetwork(WADirectActivity.this)) {
            MyApplication.activity = this;
            prefs = new Prefs(this);
            init();
            setUpEvents();
            if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                native_container_insta.setVisibility(View.GONE);
                thirdBannerContainer.setVisibility(View.GONE);
            }
            else {
                AppUtils.getShowAdData(WADirectActivity.this, TAG, adContainer, native_container_insta);
                AppUtils.getShowThirdPartyAdData(WADirectActivity.this, TAG, thirdBannerContainer, null);
            }
        } else {
            NoInternetConnection(WADirectActivity.this);
        }
    }

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        native_container_insta = (LinearLayout) findViewById(R.id.native_container_insta);
        //   thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        countryPicker = (CountryCodePicker) findViewById(R.id.countryPicker);
        etMobileNo = (EditText) findViewById(R.id.etMobileNo);
        btnOpenChat = (Button) findViewById(R.id.btnOpenChat);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        iconInsta = (ImageView) findViewById(R.id.iconInsta);
        iconHelp.setVisibility(View.GONE);
    }

    private void setUpEvents() {
        txtTitle.setText("WhatsApp Direct");
        iconInsta.setImageDrawable(getResources().getDrawable(R.drawable.whats_direct_icon));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnOpenChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNo = etMobileNo.getText().toString();
                if (TextUtils.isEmpty(mobileNo)) {
                    etMobileNo.setError("Please enter number");
                } else {
                    openWA(mobileNo);
                }
            }
        });
    }

    private void openWA(String mobileNo) {
        try {
            String url = String.format("whatsapp://send/?&phone=%s%s", countryPicker.getSelectedCountryCodeWithPlus(), mobileNo);
            Intent i = new Intent("android.intent.action.VIEW", Uri.parse(url));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            AppMethod.showAlert(WADirectActivity.this, "WhatsApp exist. Install latest verision of WhatsApp from play store to using this service");
        }
    }
}