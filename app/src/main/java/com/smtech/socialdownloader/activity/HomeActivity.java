package com.smtech.socialdownloader.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.utils.AppUtils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.IntentInterstitialAd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

public class HomeActivity extends Activity {

    LinearLayout linInstagram, linFacebook, linWhatsApp;
    LinearLayout linLikee, linTwitter, linShareChat;
    LinearLayout linTikTok, linRoposo, linWhatsAppDirect;
    LinearLayout linGallery, linFeedback, linAbout;
    LinearLayout linMoreApps, linShare, linRate;
    LinearLayout adContainer;
    LinearLayout adContainernative;
    LinearLayout thirdBannerContainer;
    private static final String TAG = "HomeActivity";

    Button subscribe;
    BillingClient billingClient;
    Prefs prefs;
    ScrollView mainscroll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_home);
       setContentView(R.layout.new_homeactivity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(HomeActivity.this);
        prefs = new Prefs(this);


        if (isConnectedNetwork(HomeActivity.this)) {
            MyApplication.activity = this;
            init();
            setUpEvents();

            if (prefs.getPremium() == 1) {
                adContainer.setVisibility(View.GONE);
                adContainernative.setVisibility(View.GONE);
              //  thirdBannerContainer.setVisibility(View.GONE);

            } else {
                AppUtils.getShowAdData(HomeActivity.this, TAG, adContainer, adContainernative);
              //  AppUtils.getShowThirdPartyAdData(HomeActivity.this, TAG, thirdBannerContainer, null);
            }

        } else {
            NoInternetConnection(HomeActivity.this);
        }

        billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener(
                        new PurchasesUpdatedListener() {
                            @Override
                            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                                    for (Purchase purchase : list) {
                                        verifySubPurchase(purchase);
                                    }
                                }
                            }
                        }
                ).build();

        establishConnection();



        mainscroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = mainscroll.getScrollY(); // For ScrollView
                int scrollX = mainscroll.getScrollX(); // For HorizontalScrollView
                if(scrollX<scrollY){
                    adContainer.setVisibility(View.VISIBLE);
                }
                else  {
                    adContainer.setVisibility(View.GONE);
                }
            }
        });

    }

    private void init() {
        AppUtils.createFileFolder();
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
      //  thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container);
        linInstagram = (LinearLayout) findViewById(R.id.linInstagram);
        linFacebook = (LinearLayout) findViewById(R.id.linFacebook);
        linWhatsApp = (LinearLayout) findViewById(R.id.linWhatsApp);
        linLikee = (LinearLayout) findViewById(R.id.linLikee);
        // linTwitter = (LinearLayout) findViewById(R.id.linTwitter);
        linShareChat = (LinearLayout) findViewById(R.id.linShareChat);
        linTikTok = (LinearLayout) findViewById(R.id.linTikTok);
        linRoposo = (LinearLayout) findViewById(R.id.linRoposo);
        linWhatsAppDirect = (LinearLayout) findViewById(R.id.linWhatsAppDirect);
        linGallery = (LinearLayout) findViewById(R.id.linGallery);
        linFeedback = (LinearLayout) findViewById(R.id.linFeedback);
        linAbout = (LinearLayout) findViewById(R.id.linAbout);
        linMoreApps = (LinearLayout) findViewById(R.id.linMoreApps);
        linShare = (LinearLayout) findViewById(R.id.linShare);
        linRate = (LinearLayout) findViewById(R.id.linRate);
        subscribe = findViewById(R.id.subscribe);
        mainscroll = findViewById(R.id.mainscroll);
    }

    private void sendFeedback(String osName) {
        String[] TO = {getString(R.string.feedback_mail_id)};
        String title = " Device : " + Build.DEVICE + " " + " Model : " + Build.MODEL + " " + " Android Version : " + osName;
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        emailIntent.putExtra(Intent.EXTRA_TEXT, title);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(HomeActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }



    private void setUpEvents() {
        final String appPackageName = getPackageName();
        Field[] fields = Build.VERSION_CODES.class.getFields();
        String osName = fields[Build.VERSION.SDK_INT].getName();
        linInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, InstagramActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, FacebookActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, WhatsAppActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linLikee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, LikeeActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
      /*  linTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, TwitterActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });*/
        linShareChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ShareChatActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linTikTok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, TikTokActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linRoposo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RoposoActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linWhatsAppDirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, WADirectActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, GalleryActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback(osName);
            }
        });
        linAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AboutUsActivity.class);
                IntentInterstitialAd(HomeActivity.this, intent);
            }
        });
        linMoreApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Pahal+Technology&hl=en-GB")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Pahal+Technology&hl=en-GB")));
                }
            }
        });
        linShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Hey check out " + getString(R.string.app_name) + " app at: https://play.google.com/store/apps/details?id=" + appPackageName);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        linRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(HomeActivity.this,ExitAppActivity.class);
        startActivity(i);
    }

    void establishConnection() {

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    showProducts();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                establishConnection();
            }
        });
    }

    void showProducts() {
        List<String> skuList = new ArrayList<>();
        skuList.add("01");
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSkuDetailsResponse(@NonNull BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                            // Process the result.
                            for (SkuDetails skuDetails : skuDetailsList) {
                                if (skuDetails.getSku().equals("01")) {
                                    //Now update the UI
                                    // txt_price.setText(skuDetails.getPrice() + " Per Month");
                                    subscribe.setOnClickListener(view -> {
                                        launchPurchaseFlow(skuDetails);
                                    });
                                }
                            }
                        }
                    }
                });


    }

    void launchPurchaseFlow(SkuDetails skuDetails) {

        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build();

        billingClient.launchBillingFlow(HomeActivity.this, billingFlowParams);
    }


    void verifySubPurchase(Purchase purchases) {

        AcknowledgePurchaseParams acknowledgePurchaseParams = AcknowledgePurchaseParams
                .newBuilder()
                .setPurchaseToken(purchases.getPurchaseToken())
                .build();

        billingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
            @Override
            public void onAcknowledgePurchaseResponse(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Toast.makeText(HomeActivity.this, "You are a premium user now", Toast.LENGTH_SHORT).show();
                    //updateUser();

                    //Setting premium to 1
                    // 1 - premium
                    //0 - no premium
                    prefs.setPremium(1);
                }
            }
        });

        Log.d(TAG, "Purchase Token: " + purchases.getPurchaseToken());
        Log.d(TAG, "Purchase Time: " + purchases.getPurchaseTime());
        Log.d(TAG, "Purchase OrderID: " + purchases.getOrderId());
    }


    protected void onResume() {
        super.onResume();

        billingClient.queryPurchasesAsync(
                BillingClient.SkuType.SUBS,
                new PurchasesResponseListener() {
                    @Override
                    public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> list) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            for (Purchase purchase : list) {
                                if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED && !purchase.isAcknowledged()) {
                                    verifySubPurchase(purchase);
                                }
                            }
                        }
                    }
                }
        );

    }





}