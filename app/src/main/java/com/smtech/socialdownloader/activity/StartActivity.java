package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.IntentInterstitialAd;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.utils.AppUtils;

public class StartActivity extends AppCompatActivity {

    private final String TAG = "StartActivity";
    private LinearLayout StartLinearLayout;
    private Intent intent;
    private LinearLayout banner_container;
    private LinearLayout native_container;
    boolean isinstalled;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(StartActivity.this);

        if (isConnectedNetwork(StartActivity.this)) {

            // banner_container = (LinearLayout)findViewById(R.id.banner_container);
            native_container = (LinearLayout)findViewById(R.id.native_container);
            AppUtils.getShowAdData(StartActivity.this, TAG, native_container,null);

            StartLinearLayout = (LinearLayout) findViewById(R.id.StartLinearLayout);
            StartLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int PolicyStatus = PrefData.pref.getInt(PrefData.PolicyST, 0);
                    if (PolicyStatus == 0) {
                        intent = new Intent(StartActivity.this, PolicyActivity.class);
                        IntentInterstitialAd(StartActivity.this,intent);
                    } else {
                        intent = new Intent(StartActivity.this, LetsStartActivity.class);
                        IntentInterstitialAd(StartActivity.this,intent);
                    }
                }
            });
        }else {
            NoInternetConnection(StartActivity.this);
        }
    }

    @Override
    public void onBackPressed()
    {
        intent = new Intent(StartActivity.this, ExitAppActivity.class);
        startActivity(intent);

    }

}