package com.smtech.socialdownloader.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;



import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.adapter.UPageAadapter;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

public class InstagramActivity extends AppCompatActivity {

    private EditText etUrl;
    private Button btnPaste,btnOKinsta;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2;
    private TextView txtTitle, txtAppTitle;
    private static final int FILE_PERMISSION_REQUEST_CODE = 1642;
    boolean isShowTutorial = false;
    private CardView cardTutorial;
    LinearLayout adContainer, adContainernative;
    LinearLayout thirdBannerContainer;

    TabLayout utabLayout;
    TabItem insta_video_tab, insta_status_tab;
    ViewPager Uvpager;
    UPageAadapter upageAadapter;

    private static final String TAG = "InstagramActivity";
    private TextView tvLogin;
    Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        utabLayout = findViewById(R.id.insta_tab);
        insta_video_tab = findViewById(R.id.insta_video_tab);
        insta_status_tab = findViewById(R.id.insta_status_tab);
        //adContainer = (LinearLayout) findViewById(R.id.banner_container);

        adContainernative = (LinearLayout) findViewById(R.id.native_container);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        iconBack = (ImageView) findViewById(R.id.iconBack);


        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iconHelp = (ImageView) findViewById(R.id.iconHelpp);

        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alerdialg = new AlertDialog.Builder(InstagramActivity.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view1 = inflater.inflate(R.layout.icon_help_insta,null);
                alerdialg.setView(view1);
                alerdialg.setCancelable(false);

                AlertDialog dialogbox = alerdialg.create();

                cardTutorial = view1.findViewById(R.id.cardTutorial);
                imgTutorial1 = view1.findViewById(R.id.imgTutorial1);
                imgTutorial2 = view1.findViewById(R.id.imgTutorial2);
                btnOKinsta = view1.findViewById(R.id.btnOKinsta);


                imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_insta_1));
                imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_insta_2));

                btnOKinsta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogbox.dismiss();
                    }
                });

                dialogbox.show();
            }
        });

        /*iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }
            }
        });*/


        Uvpager = findViewById(R.id.InstaVpager);
        upageAadapter = new UPageAadapter(getSupportFragmentManager(), utabLayout.getTabCount());
        Uvpager.setAdapter(upageAadapter);

        new PrefData(InstagramActivity.this);

        if (isConnectedNetwork(InstagramActivity.this)) {
            MyApplication.activity = this;

        } else {
            NoInternetConnection(InstagramActivity.this);
        }

        utabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Uvpager.setCurrentItem(tab.getPosition());
                if(tab.getPosition()==0 || tab.getPosition()==1){
                    upageAadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Uvpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(utabLayout));
    }



}

   /* private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InstagramActivity.this, IntagramStoryActivity.class);
                startActivityForResult(intent, 100);
            }
        });
    }

    private void setUpEvents() {
        txtTitle.setText("Instagram");
        txtAppTitle.setText("Instagram");
        appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_insta));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_insta_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_insta_2));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }
            }
        });
        etUrl.setHint("Enter or Paste Instagram URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(InstagramActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String instaPostUrl = etUrl.getText().toString();
                if (TextUtils.isEmpty(instaPostUrl)) {
                    etUrl.setError("Enter Instagram URL");
                } else {
                    try {
                        if (new URL(instaPostUrl).getHost().contains("instagram")) {
                            if (!instaPostUrl.startsWith("https://www.instagram.com/")) {
                                instaPostUrl = instaPostUrl.substring(instaPostUrl.indexOf("https://www.instagram.com/"));
                            }
                            generateInstaDowURL(instaPostUrl);
                        } else {
                            etUrl.setError("Invalid Instagram URL");
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void generateInstaDowURL(String url) {
        url = getUrlWithoutParameters(url) + "?__a=1";
        HttpRequest _request = new HttpRequest(InstagramActivity.this, 1, url, "INSTAGRAM_URL", new RetrofitHttpListener() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String WSTag, String response) {
                String[] FILE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (AppMethod.hasPermissions(InstagramActivity.this, FILE_PERMISSIONS)) {
                    new InstaStartDownloadsAsync(InstagramActivity.this, response, new InstaStartDownloadsAsync.EventListener() {
                        @Override
                        public void onSuccess(boolean success) {
                            if (success) {
                                Toast.makeText(InstagramActivity.this, "Downloading Started...", Toast.LENGTH_SHORT).show();
                                etUrl.setText("");
                                if (AppUtils.isAdmobAvailable(InstagramActivity.this)) {
                                    AdmobAdUtils.showAdmobInterstitialAd(InstagramActivity.this, true, true);
                                } else {
                                    FacebookAdUtils.showFbInterstitialAd(InstagramActivity.this, true);
                                }
                            } else {
                                AppMethod.showAlertWithOk(InstagramActivity.this, getString(R.string.app_name), "Sorry! Unable to dowmload media!");
                            }
                        }
                    }).execute();
                } else {
                    requestPermissions(FILE_PERMISSIONS, FILE_PERMISSION_REQUEST_CODE);
                }
            }
        });
        _request.execute(url);
    }

    private String getUrlWithoutParameters(String str) {
        try {
            URI uri = new URI(str);
            return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), (String) null, uri.getFragment()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(InstagramActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return "";
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case FILE_PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    Toast.makeText(InstagramActivity.this, "Try now to view/download media!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }*/
