package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.smtech.socialdownloader.ImageAdapter;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.Status;
import com.smtech.socialdownloader.Whtsapp_new.ViewPagerAdapter;
import com.smtech.socialdownloader.Whtsapp_new.fragment.PictureFragment;
import com.smtech.socialdownloader.Whtsapp_new.fragment.SavedFragment;
import com.smtech.socialdownloader.Whtsapp_new.fragment.VideoFragment;
import com.smtech.socialdownloader.Whtsapp_new.utils.PrefManager;
import com.smtech.socialdownloader.adapter.FileGridAdapter;
import com.smtech.socialdownloader.utils.AppUtils;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

public class WhatsAppActivity extends AppCompatActivity{

    private ImageView iconHelp;
    private ImageView iconBack;
    LinearLayout llNoData;

    RecyclerView rvStatus;
    ArrayList<Status> statusMediaList;
    FileGridAdapter whatsappStatusAdapter;
    private TextView txtTitle;
    LinearLayout llBottomOption;
    LinearLayout llShare, llDownload, llDelete;
    String[] FILE_PERMISSIONS;
    private final Handler handler = new Handler();

    boolean showOptions = false;
    int selectedItemCnt = 0;

    private static final int FILE_PERMISSION_REQUEST_CODE = 4665;
    private String targetPath;
    private static final String TAG = "WhatsAppActivity";
    LinearLayout adContainer;
    LinearLayout thirdBannerContainer;
    private static final String MANAGE_EXTERNAL_STORAGE_PERMISSION = "android:manage_external_storage";
    private final List<Status> imagesList = new ArrayList<>();
    private ImageAdapter imageAdapter;
    Prefs prefs;

//////////////////////////

    TabLayout tabs;

    ViewPager whatsappStatusViewPager;
    private CardView cardTutorial;
    ImageView imgTutorial1, imgTutorial2;
    Button btnOKinsta;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whatsapp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(WhatsAppActivity.this);

        prefs = new Prefs(this);
        if (isConnectedNetwork(WhatsAppActivity.this)) {
            MyApplication.activity = this;
          //  init();
            //setUpEvents();
           //getStatus();
            setUpTabs();
            checkPermission();

            /*if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                thirdBannerContainer.setVisibility(View.GONE);
            }
            else {
                AppUtils.getShowAdData(WhatsAppActivity.this, TAG, adContainer, null);
                AppUtils.getShowThirdPartyAdData(WhatsAppActivity.this, TAG, thirdBannerContainer, null);
            }*/


        } else {
            NoInternetConnection(WhatsAppActivity.this);
        }


        tabs = findViewById(R.id.tabs);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText("WhatsApp");
        iconBack = findViewById(R.id.whtsapp_iconBack);
        whatsappStatusViewPager = findViewById(R.id.whatsappStatusViewPager);

        setTitle("Status Saver");


        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });





    }

    public void onRestart() {
        super.onRestart();
        setUpTabs();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    private void setUpTabs() {
        new Handler().post(new com.smtech.socialdownloader.Whtsapp_new.DashboardActivity$$ExternalSyntheticLambda1(this, new ViewPagerAdapter(getSupportFragmentManager())));
    }

    /* renamed from: lambda$setUpTabs$1$com-larntech-whatsappstatussaver-ui-activity-DashboardActivity */
    public /* synthetic */ void mo25732x196f4921(ViewPagerAdapter viewPagerAdapter) {
        viewPagerAdapter.addFragment(PictureFragment.newInstance(), "IMAGES");
        viewPagerAdapter.addFragment(VideoFragment.newInstance(), "VIDEOS");
        viewPagerAdapter.addFragment(SavedFragment.newInstance(), "SAVED");
        this.whatsappStatusViewPager.setAdapter(viewPagerAdapter);
        this.tabs.setupWithViewPager(this.whatsappStatusViewPager);
    }


    /*private void init() {
        AppUtils.createFileFolder();
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
       llNoData = (LinearLayout) findViewById(R.id.llNoData);
       llNoData.setVisibility(View.GONE);

        rvStatus = (RecyclerView) findViewById(R.id.rvStatus);
        rvStatus.setLayoutManager(new GridLayoutManager(WhatsAppActivity.this, 3));
    *//*    statusMediaList = new ArrayList<>();
        whatsappStatusAdapter = new FileGridAdapter(WhatsAppActivity.this, statusMediaList);*//*
     //   rvStatus.setAdapter(whatsappStatusAdapter);

        llBottomOption = (LinearLayout) findViewById(R.id.llBottomOption);
        llBottomOption.setVisibility(View.GONE);
        llShare = (LinearLayout) findViewById(R.id.llShare);
        llDownload = (LinearLayout) findViewById(R.id.llDownload);
        llDelete = (LinearLayout) findViewById(R.id.llDelete);
        llDelete.setVisibility(View.GONE);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        iconHelp.setVisibility(View.GONE);
    }*/

//    private void setUpEvents() {
//        txtTitle.setText("WhatsApp");
//        /*iconBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });*/
//        ItemClickSupport.addTo(rvStatus).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                if (showOptions) {
//                    imagesList.get(position).setVideo(!statusMediaList.get(position).isVideo());
//
//                    imageAdapter.notifyItemChanged(position);
//                    if (imagesList.get(position).isVideo()) {
//                        selectedItemCnt++;
//                    } else {
//                        selectedItemCnt--;
//                    }
//
//                    if (selectedItemCnt == 0) {
//                        showOptions = false;
//                        if (llBottomOption.getVisibility() == View.VISIBLE) {
//                            llBottomOption.setVisibility(View.GONE);
//                        }
//                    }
//                } else {
//                    ArrayList<String> filePathList = new ArrayList<>();
//                    for (int i = 0; i < imagesList.size(); i++) {
//                        filePathList.add(imagesList.get(i).getPath());
//                    }
//
//                    Intent intent = new Intent(WhatsAppActivity.this, MediaPreviewActivity.class);
//                    intent.putExtra("filePathList", filePathList);
//                    intent.putExtra("initPos", position);
//                    intent.putExtra("fromWAStatus", true);
//                    IntentInterstitialAd(WhatsAppActivity.this, intent);
//                }
//            }
//        });
//        ItemClickSupport.addTo(rvStatus).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
//                if (!showOptions) {
//                    showOptions = true;
//                    selectedItemCnt = 1;
//                    statusMediaList.get(position).setVideo(true);
//                    imageAdapter.notifyItemChanged(position);
//                    if (llBottomOption.getVisibility() == View.GONE) {
//                        llBottomOption.setVisibility(View.VISIBLE);
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });
//        llShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    Intent intent = new Intent();
//                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
//                    intent.setType("*/*");
//
//                    ArrayList<Uri> files = new ArrayList<Uri>();
//                    for (int i = 0; i < imagesList.size(); i++) {
//                        if (imagesList.get(i).isVideo()) {
//                            File file = new File(imagesList.get(i).getPath());
//                            Uri uri = Uri.fromFile(file);
//                            files.add(uri);
//                        }
//                    }
//                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
//                    startActivity(intent);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        llDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String[] APP_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
//                if (AppMethod.hasPermissions(WhatsAppActivity.this, APP_PERMISSIONS)) {
//                    ArrayList<Status> list = new ArrayList<>();
//                    for (int i = 0; i < imagesList.size(); i++) {
//                        if (imagesList.get(i).isVideo()) {
//                            list.add(imagesList.get(i));
//                        }
//                    }
//                    if (list != null && list.size() > 0) {
//
//                        new CopyAllMediaAsync(WhatsAppActivity.this, list, AppUtils.RootDirectoryWhatsAppShow, new CopyAllMediaAsync.CopyMediaListener() {
//                            @Override
//                            public void onResult(boolean success) {
//                                if (success) {
//                                    Toast.makeText(WhatsAppActivity.this, "Status Saved Successfully!", Toast.LENGTH_SHORT).show();
//                                   getStatus();
//                                    //  Log.i(TAG, "onClick: "+list.size());
//                                    if (AppUtils.isAdmobAvailable(WhatsAppActivity.this)) {
//                                        AdmobAdUtils.showAdmobInterstitialAd(WhatsAppActivity.this, true, true);
//                                    } else {
//                                        FacebookAdUtils.showFbInterstitialAd(WhatsAppActivity.this, true);
//                                    }
//                                } else {
//                                    Toast.makeText(WhatsAppActivity.this, AppConstant.SOMETHING_WRONG_TRY_AGAIN, Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }).execute();
//                    }
//                } else {
//                    Toast.makeText(WhatsAppActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }

   /* @SuppressLint("NewApi")
    private void getData() {
        showOptions = false;
        selectedItemCnt = 0;
        llBottomOption.setVisibility(View.GONE);
        FILE_PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(WhatsAppActivity.this, FILE_PERMISSIONS)) {

            File dir1 = new File(Environment.getExternalStorageDirectory(),AppUtils.whatsAppDirectory);
            File dir = new File(Environment.getExternalStorageDirectory(), AppUtils.waStatusDirectory);

            if (dir.exists() || dir1.exists())
            {
                targetPath = AppUtils.waStatusDirectory;
            }
            else
            {
                targetPath = AppUtils.whatsAppDirectory;
            }

            new GetAllFilesAsync(WhatsAppActivity.this, targetPath, new GetAllFilesAsync.EventListener() {

                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onResult(ArrayList<MediaModel> mediaList) {
                    if (mediaList != null && mediaList.size() > 0) {
                       statusMediaList.clear();
                        statusMediaList.addAll(mediaList);
                        Log.i(TAG, "statusmedialist: "+statusMediaList.size());
                        whatsappStatusAdapter.notifyDataSetChanged();
                        llNoData.setVisibility(View.GONE);
                    } else {
                        llNoData.setVisibility(View.VISIBLE);
                    }
                }
            }).execute();
        } else {
            //arePermissionDenied();
           // requestPermission();
         requestPermissions(FILE_PERMISSIONS, FILE_PERMISSION_REQUEST_CODE);
         //checkStorageApi30();

        }


    }*/

   /* private void getStatus() {

        if (Common.STATUS_DIRECTORY.exists()) {

            execute(Common.STATUS_DIRECTORY);

        } else if (Common.STATUS_DIRECTORY_NEW.exists()) {

            execute(Common.STATUS_DIRECTORY_NEW);

        } else {
            Toast.makeText(this, "Can't find", Toast.LENGTH_SHORT).show();
           // swipeRefreshLayout.setRefreshing(false);
        }

    }*/

    /*private void execute(File wAFolder) {
        new Thread(() -> {
            File[] statusFiles;
            statusFiles = wAFolder.listFiles();
          //  imagesList.clear();

            if (statusFiles != null && statusFiles.length > 0) {

                Arrays.sort(statusFiles);
                for (File file : statusFiles) {
                    Status status = new Status(file, file.getName(), file.getAbsolutePath());

                    if ((status.isVideo()) || (!status.isVideo() && status.getTitle().endsWith(".jpg"))) {
                        imagesList.add(status);
                    }

                }

                handler.post(() -> {

                    if (imagesList.size() <= 0) {
                        Toast.makeText(this, "Data not find.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Data find successfully.", Toast.LENGTH_SHORT).show();
                    }

                    imageAdapter = new ImageAdapter(imagesList);
                    rvStatus.setAdapter(imageAdapter);
                    imageAdapter.notifyDataSetChanged();
                 //   progressBar.setVisibility(View.GONE);
                });

            } else {

                handler.post(() -> {
                  //  progressBar.setVisibility(View.GONE);
                   *//* messageTextView.setVisibility(View.VISIBLE);
                    messageTextView.setText(R.string.no_files_found);*//*
                    llNoData.setVisibility(View.VISIBLE);
                });

            }
            //swipeRefreshLayout.setRefreshing(false);
        }).start();
    }*/
    public void openNextActivity() {
        if (Build.VERSION.SDK_INT < 29) {
            proceedToDashboard();
        } else if (PrefManager.isPermission().booleanValue()) {
            proceedToDashboard();
        } else if (isAppInstalled("com.whatsapp")) {
            checkWhatsApp();
            checkisinstalledfirst();
        } else if (isAppInstalled("com.whatsapp.w4b")) {
            checkWhatsAppBs();
            checkisinstalledfirst();
        }
    }

    private void proceedToDashboard() {
        PrefManager.setFirstLaunch(false);
      //  startActivity(new Intent(this, WhatsAppActivity.class));

    }



    public void onDestroy() {
        super.onDestroy();
    }

    private void checkPermission() {
        Dexter.withContext(this).withPermission("android.permission.WRITE_EXTERNAL_STORAGE").withListener(new PermissionListener() {
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                WhatsAppActivity.this.openNextActivity();
            }

            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                WhatsAppActivity.this.showSettingsDialog();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permissionRequest, PermissionToken permissionToken) {

            }

        }).check();
    }


    public void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Storage Permissions");
        builder.setMessage("This app needs Storage permission to access status image/video. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                WhatsAppActivity.this.openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }


    public void openSettings() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getPackageName(), null));
        startActivityForResult(intent, 101);
    }

    private void checkWhatsApp() {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        intent.putExtra("android.provider.extra.INITIAL_URI", Uri.parse("content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fmedia/document/primary%3AAndroid%2Fmedia%2Fcom.whatsapp%2FWhatsApp%2FMedia%2F.Statuses"));
        startActivityForResult(intent, 10001);
    }

    private void checkWhatsAppBs() {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        intent.putExtra("android.provider.extra.INITIAL_URI", Uri.parse("content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fmedia/document/primary%3AAndroid%2Fmedia%2Fcom.whatsapp.w4b%2FWhatsApp%20Business%2FMedia%2F.Statuses"));
        startActivityForResult(intent, 10002);
    }

    @SuppressLint("WrongConstant")
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 501) {
            openNextActivity();
        } else if (i == 10001 && i2 == -1) {
            if (intent != null) {
                Uri data = intent.getData();
                PrefManager.setUrl(data.toString());
                PrefManager.setPermission(true);
                int flags = intent.getFlags() & 3;
                if (Build.VERSION.SDK_INT >= 19) {
                    getContentResolver().takePersistableUriPermission(data, flags);
                }
                if (isAppInstalled("com.whatsapp.w4b")) {
                    checkWhatsAppBs();
                } else {
                    openNextActivity();
                }
            }
        } else if (i == 10002) {
            if (i2 != -1) {
                openNextActivity();
            } else if (intent != null) {
                Uri data2 = intent.getData();
                PrefManager.setUrlw4b(data2.toString());
                PrefManager.setPermission(true);
                int flags2 = intent.getFlags() & 3;
                if (Build.VERSION.SDK_INT >= 19) {
                    getContentResolver().takePersistableUriPermission(data2, flags2);
                }
                openNextActivity();
            }
        }
        if (i2 == 0) {
            Toast.makeText(this, " You need to accept permission to continue ", 0).show();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    WhatsAppActivity.this.openNextActivity();
                }
            }, 1500);
        }
    }

    private boolean isAppInstalled(String str) {
        try {
            getPackageManager().getPackageInfo(str, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }


    private void checkisinstalledfirst() {
        Intent intent = new Intent(WhatsAppActivity.this, animation_permission.class);
        startActivity(intent);
    }

    }
