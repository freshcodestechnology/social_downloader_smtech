package com.smtech.socialdownloader.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import com.google.gson.Gson;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.RetrofitUtils.HttpRequest;
import com.smtech.socialdownloader.RetrofitUtils.RetrofitHttpListener;
import com.smtech.socialdownloader.globle.AppConstant;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.model.AdDetail.AdConfig;
import com.smtech.socialdownloader.model.AdDetail.AdData;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.MyApplication;

import java.util.Date;

public class SplashActivity extends Activity {

    private static final int PERMISSION_REQUEST_CODE = 1001;
    private static final String TAG = "SplashActivity";
    LinearLayout adContainer;

    private AppOpenAd appOpenAd = null;
    private long loadTime = 0;

    Intent intent;

    private AppOpenAd.AppOpenAdLoadCallback loadCallback;
    public static boolean isShowingAd = false;
    private AdConfig adConfig;
    BillingClient billingClient;
    Prefs prefs;

    ImageView maintitle,subtitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       // setContentView(R.layout.activity_splash);
        setContentView(R.layout.new_splash);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(SplashActivity.this);
        prefs = new Prefs(this);


        if (isConnectedNetwork(SplashActivity.this)) {
            MyApplication.activity = this;

            init();
            AppUtils.getShowAdData(SplashActivity.this, TAG, adContainer, null);
        } else {
            NoInternetConnection(SplashActivity.this);
        }



     Glide.with(this).load(R.drawable.newresized_splashtitle).into(maintitle);
     Glide.with(this).load(R.drawable.splash_subsubtitle).into(subtitle);



    }

    private void init() {
        maintitle = findViewById(R.id.apptitle);
       subtitle = findViewById(R.id.subtitle);
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        callAdConfig();
        callAdData();
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.R)
            @Override
            public void run() {
                String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE};
                if (AppMethod.hasPermissions(SplashActivity.this, PERMISSIONS)) {
                    AppMethod.setIntegerPreference(SplashActivity.this, AppConstant.PREF_MEDIA_COUNT, 0);
                    adConfig = AppUtils.getAdConfigFromPreference(SplashActivity.this);
                    Log.e(TAG, "ffff " + adConfig);
                    if (adConfig.getAdProvider().getAdStatus() == 1 && !adConfig.getAdProvider().getAdProvider().equalsIgnoreCase(AppConstant.facebook) && adConfig.getAdProvider().getIsAppOpen() == 1) {
                        ShowOpenAds();
                    } else {
                       /* Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                        gotoNextPage();
                    }
                } else {
                    ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);

                }
            }
        }, AppConstant.SPLASH_TIME_OUT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    AppMethod.setIntegerPreference(SplashActivity.this, AppConstant.PREF_MEDIA_COUNT, 0);
                    adConfig = AppUtils.getAdConfigFromPreference(SplashActivity.this);
                    Log.e(TAG, "jjj"+adConfig);
                    if (adConfig.getAdProvider().getAdStatus() == 1 && !adConfig.getAdProvider().getAdProvider().equalsIgnoreCase(AppConstant.facebook) && adConfig.getAdProvider().getIsAppOpen() == 1) {
                        ShowOpenAds();
                    } else {
                        /*Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                        gotoNextPage();
                    }
                } else {
                    SplashActivity.this.finish();
                }
                break;
        }
    }

    public void gotoNextPage() {
        if (isFirstInstall(this) ) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putBoolean("firstLaunch", false).apply();
            intent = new Intent(SplashActivity.this, PolicyActivity.class);
            intent.putExtra("status",adConfig.getAdProvider().getDisplay_extra_activities().intValue());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return;
        }

        if(prefs.getPremium()==1)
        {
            appOpenAd = null;
            isShowingAd = false;
            MyApplication.timer.cancel();
            intent = new Intent(SplashActivity.this, HomeActivity.class);

        }

        if(adConfig.getAdProvider().getDisplay_extra_activities() == 1)
        {
            intent = new Intent(SplashActivity.this, StartActivity.class);
            intent.putExtra("status",adConfig.getAdProvider().getDisplay_extra_activities().intValue());
        }
        else{
            intent = new Intent(SplashActivity.this, HomeActivity.class);
            intent.putExtra("status",adConfig.getAdProvider().getDisplay_extra_activities().intValue());
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

       /* if (adConfig.getAdProvider().getDisplay_extra_activities() == 1) {
            intent = new Intent(SplashActivity.this, StartActivity.class);


        } else {
            intent = new Intent(SplashActivity.this, HomeActivity.class);
            intent.putExtra("status",adConfig.getAdProvider().getDisplay_extra_activities().intValue());

        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/
    }


    private void callAdConfig() {
        String url = AppConstant.AD_CONFIG + getPackageName();
        HttpRequest httpRequest = new HttpRequest(SplashActivity.this, 1, url, "AD_CONFIG", new RetrofitHttpListener() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String WSTag, String response) {
                try{
                    AdConfig adConfig = new Gson().fromJson(response, AdConfig.class);
                    Log.e(TAG, "AllonResponse: "+response);
                    try {
                        ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
                        ai.metaData.putString("com.google.android.gms.ads.APPLICATION_ID", adConfig.getAdProvider().getGoogleAppId());
                        ai.metaData.putString("com.facebook.sdk.ApplicationId", adConfig.getAdProvider().getFacebookAppId());
                    } catch (PackageManager.NameNotFoundException e) {
                    } catch (NullPointerException e) {
                    } catch (Exception e) {
                    }

                    AppMethod.setPreferenceObject(SplashActivity.this, adConfig, AppConstant.PREF_AD_CONFIG);

                    if (adConfig.getAdProvider().getAdProvider().equalsIgnoreCase(AppConstant.facebook)) {
                        AppMethod.setStringPreference(SplashActivity.this, AppConstant.PREF_AD_TYPE, AppConstant.facebook);
                    } else {
                        AppMethod.setStringPreference(SplashActivity.this, AppConstant.PREF_AD_TYPE, AppConstant.admob);
                    }

                } catch (NullPointerException e) {
                    e.toString();
                }

            }

        });
        httpRequest.execute(url);
    }

    private void callAdData() {
        String url = AppConstant.AD_DATA + getPackageName();
        HttpRequest httpRequest = new HttpRequest(SplashActivity.this, 1, url, "SCREEN_CONFIG", new RetrofitHttpListener() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String WSTag, String response) {

                //Log.e(TAG, "onResponse: " + response.toString());

                AdData adData = new Gson().fromJson(response, AdData.class);
                AppMethod.setPreferenceObject(SplashActivity.this, adData, AppConstant.PREF_AD_DATA);

            }
        });
        httpRequest.execute(url);
    }

    /**
     * Creates and returns ad request.
     */
    private AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    /**
     * Utility method that checks if ad exists and can be shown.
     */
    public boolean isAdAvailable() {
        return appOpenAd != null && wasLoadTimeLessThanNHoursAgo(4);
    }

    /**
     * Utility method to check if ad was loaded more than n hours ago.
     */
    private boolean wasLoadTimeLessThanNHoursAgo(long numHours) {
        long dateDifference = (new Date()).getTime() - loadTime;
        long numMilliSecondsPerHour = 3600000;
        return (dateDifference < (numMilliSecondsPerHour * numHours));
    }

    public void ShowOpenAds() {
        if(prefs.getPremium()==1){
            gotoNextPage();
        }
        else {


            loadCallback =
                    new AppOpenAd.AppOpenAdLoadCallback() {
                        /**
                         * Called when an app open ad has loaded.
                         *
                         * @param ad the loaded app open ad.
                         */
                        @Override
                        public void onAdLoaded(AppOpenAd ad) {

                            appOpenAd = ad;
                            loadTime = (new Date()).getTime();

                            if (!isShowingAd && isAdAvailable()) {

                                FullScreenContentCallback fullScreenContentCallback =
                                        new FullScreenContentCallback() {
                                            @Override
                                            public void onAdDismissedFullScreenContent() {

                                                // Set the reference to null so isAdAvailable() returns false.
                                                appOpenAd = null;
                                                isShowingAd = false;

                                           /* Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);*/

                                                gotoNextPage();
                                            }

                                            @Override
                                            public void onAdFailedToShowFullScreenContent(AdError adError) {

                                          /*  Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);*/

                                                gotoNextPage();
                                            }

                                            @Override
                                            public void onAdShowedFullScreenContent() {

                                                isShowingAd = true;
                                            }
                                        };

                                appOpenAd.setFullScreenContentCallback(fullScreenContentCallback);
                                appOpenAd.show(SplashActivity.this);
                            }
                        }

                        /**
                         * Called when an app open ad has failed to load.
                         *
                         * @param loadAdError the error.
                         */
                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            // Handle the error.

                      /*  Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/

                            gotoNextPage();
                        }

                    };
            AdRequest request = getAdRequest();

            String adId = adConfig.getAdProvider().getAdmobAppOpen() == null ? SplashActivity.this.getApplicationContext().getString(R.string.admob_app_open) : adConfig.getAdProvider().getAdmobAppOpen();

            AppOpenAd.load(
                    SplashActivity.this, adId, request,
                    AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
        }
    }

    public static boolean isFirstInstall(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("firstLaunch", true);

    }


}