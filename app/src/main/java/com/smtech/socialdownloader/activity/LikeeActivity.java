package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.Constant.NoInternetConnection;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.RetrofitUtils.CommonClassForAPI;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FacebookAdUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;
import com.smtech.socialdownloader.view.OnSingleClickListener;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LikeeActivity extends Activity {

    private EditText etUrl;
    private Button btnPaste,btnOKinsta;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2,iconInsta;
    private TextView txtTitle, txtAppTitle;
    CommonClassForAPI commonClassForAPI;
    public String VideoUrl;
    AppProgressDialog appProgressDialog;
    Pattern pattern = Pattern.compile("window\\.data \\s*=\\s*(\\{.+?\\});");
    boolean isShowTutorial = false;
    private CardView cardTutorial;
    LinearLayout adContainer, adContainernative;
    LinearLayout thirdBannerContainer;
    private static final String TAG = "LikeeActivity";

    ImageView imgTutorial_1,imgTutorial_2;
    Prefs prefs;
    ScrollView fb_scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(LikeeActivity.this);
        prefs = new Prefs(this);
        if (isConnectedNetwork(LikeeActivity.this)) {
            MyApplication.activity = this;
            init();
            setUpEvents();
            if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                adContainernative.setVisibility(View.GONE);
                thirdBannerContainer.setVisibility(View.GONE);
            }
            else{
                AppUtils.getShowAdData(LikeeActivity.this, TAG, adContainer, adContainernative);
                AppUtils.getShowThirdPartyAdData(LikeeActivity.this, TAG, thirdBannerContainer, null);
            }

        } else {
            NoInternetConnection(LikeeActivity.this);
        }

        fb_scroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = fb_scroll.getScrollY(); // For ScrollView
                int scrollX = fb_scroll.getScrollX(); // For HorizontalScrollView
                if(scrollX<scrollY){
                    adContainer.setVisibility(View.VISIBLE);
                }
                else  {
                    adContainer.setVisibility(View.GONE);
                }
            }
        });
    }


    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
       // thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container_insta);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconInsta = (ImageView) findViewById(R.id.iconInsta);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        txtTitle = (TextView) findViewById(R.id.txtTitle_fb);
        fb_scroll = findViewById(R.id.fb_scroll);
        //txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        //  appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);

        imgTutorial_1 = (ImageView) findViewById(R.id.imgTutorial_1);
        imgTutorial_2 = (ImageView) findViewById(R.id.imgTutorial_2);
        btnOKinsta = (Button) findViewById(R.id.btnOKinsta_button);


        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        appProgressDialog = new AppProgressDialog(LikeeActivity.this);
        commonClassForAPI = CommonClassForAPI.getInstance(LikeeActivity.this);
        AppUtils.createFileFolder();
    }

    private void setUpEvents() {
        txtTitle.setText("Likee");
        //  txtAppTitle.setText("Likee");
        // appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_likee));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_likee_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_likee_2));

        imgTutorial_1.setImageDrawable(getResources().getDrawable(R.drawable.img_likee_1));
        imgTutorial_2.setImageDrawable(getResources().getDrawable(R.drawable.img_likee_2));


        iconInsta.setImageDrawable(getResources().getDrawable(R.drawable.likee_icon));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnOKinsta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cardTutorial.setVisibility(View.GONE);
                    }
                });

                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }

            }

        });
        etUrl.setHint("Enter or Paste Likee URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(LikeeActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                try {
                    AppUtils.createFileFolder();
                    String obj = etUrl.getText().toString();
                    if (new URL(obj).getHost().contains("likee")) {
                        Toast.makeText(LikeeActivity.this, "Downloading Started...", Toast.LENGTH_SHORT).show();
                        appProgressDialog.showDialog();
                        new callGetLikeeData().execute(obj);
                    } else {
                        etUrl.setError("Invalid Likee URL");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    class callGetLikeeData extends AsyncTask<String, Void, Document> {
        Document likeeDoc;

        callGetLikeeData() {
        }

        public void onPreExecute() {
            super.onPreExecute();
        }

        public Document doInBackground(String... strArr) {
            try {
                this.likeeDoc = Jsoup.connect(strArr[0]).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.likeeDoc;
        }

        public void onPostExecute(Document document) {
            appProgressDialog.dismissDialog();
            try {
                Matcher matcher = pattern.matcher(document.toString());
                String str = "";
                while (matcher.find()) {
                    str = matcher.group().replaceFirst("window.data = ", "").replace(";", "");
                }
                String unused = LikeeActivity.this.VideoUrl = new JSONObject(str).getString("video_url").replace("_4", "");
                if (!LikeeActivity.this.VideoUrl.equals("")) {
                    try {
                        appProgressDialog.showDialog();
                        new DownloadFileFromURL().execute(LikeeActivity.this.VideoUrl);
                        VideoUrl = "";
                        etUrl.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        DownloadFileFromURL() {
        }

        public void onPreExecute() {
            super.onPreExecute();
        }

        public String doInBackground(String... strArr) {
            try {
                URL url = new URL(strArr[0]);
                URLConnection openConnection = url.openConnection();
                openConnection.connect();
                int contentLength = openConnection.getContentLength();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openStream(), 8192);
                FileOutputStream fileOutputStream = new FileOutputStream(AppUtils.RootDirectoryLikeeShow + "/" + LikeeActivity.this.getFilenameFromURL(LikeeActivity.this.VideoUrl));
                byte[] bArr = new byte[1024];
                long j = 0;
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read != -1) {
                        j += (long) read;
                        publishProgress("" + ((int) ((100 * j) / ((long) contentLength))));
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        return null;
                    }
                }
            } catch (Exception e) {
                return null;
            }
        }

        public void onPostExecute(String str) {
            appProgressDialog.dismissDialog();
            Toast.makeText(LikeeActivity.this, "Downloaded Successfully", Toast.LENGTH_SHORT).show();
            try {
                if (Build.VERSION.SDK_INT >= 19) {
                    MediaScannerConnection.scanFile(LikeeActivity.this, new String[]{new File(AppUtils.RootDirectoryLikeeShow + "/" + LikeeActivity.this.getFilenameFromURL(LikeeActivity.this.VideoUrl)).getAbsolutePath()}, (String[]) null, new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String str, Uri uri) {
                        }
                    });
                    return;
                }
                LikeeActivity.this.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.fromFile(new File(AppUtils.RootDirectoryLikeeShow + "/" + LikeeActivity.this.getFilenameFromURL(LikeeActivity.this.VideoUrl)))));
                FirebaseAnalyticsUtils.statusDownload(LikeeActivity.this, true, "Likee");
                if (AppUtils.isAdmobAvailable(LikeeActivity.this)) {
                    AdmobAdUtils.showAdmobInterstitialAd(LikeeActivity.this, true, true);
                } else {
                    FacebookAdUtils.showFbInterstitialAd(LikeeActivity.this, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onCancelled() {
            super.onCancelled();
            appProgressDialog.dismissDialog();
        }
    }

    public String getFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath()).getName() + "";
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }
}