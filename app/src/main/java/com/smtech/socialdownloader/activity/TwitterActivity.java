package com.smtech.socialdownloader.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.RetrofitUtils.CommonClassForAPI;
import com.smtech.socialdownloader.model.TwitterResponse;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;
import com.smtech.socialdownloader.view.OnSingleClickListener;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import io.reactivex.observers.DisposableObserver;

import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.Constant.NoInternetConnection;

public class TwitterActivity extends Activity {

    private EditText etUrl;
    private Button btnPaste;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2;
    private TextView txtTitle, txtAppTitle;
    CommonClassForAPI commonClassForAPI;
    public String VideoUrl;
    AppProgressDialog appProgressDialog;
    boolean isShowTutorial = false;
    private CardView cardTutorial;
    LinearLayout adContainer, adContainernative;
    private static final String TAG = "TwitterActivity";

    private final DisposableObserver<TwitterResponse> observer = new DisposableObserver<TwitterResponse>() {
        public void onNext(TwitterResponse twitterResponse) {
           appProgressDialog.showDialog();
            try {
                runOnUiThread(() -> {
                    Toast.makeText(getApplicationContext(), "Downloading start..", Toast.LENGTH_SHORT).show();
                });
                VideoUrl = twitterResponse.getVideos().get(0).getUrl();
                if (twitterResponse.getVideos().get(0).getType().equals("image")) {
                    AppUtils.startDownload(VideoUrl, AppUtils.RootDirectoryTwitter, TwitterActivity.this, getFilenameFromURL(VideoUrl, "image"));
                    runOnUiThread(() -> {
                        etUrl.setText("");
                    });
                    return;
                }
                VideoUrl = twitterResponse.getVideos().get(twitterResponse.getVideos().size() - 1).getUrl();
                AppUtils.startDownload(VideoUrl, AppUtils.RootDirectoryTwitter, TwitterActivity.this, getFilenameFromURL(VideoUrl, "mp4"));
                runOnUiThread(() -> {
                    etUrl.setText("");
                });
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    Toast.makeText(TwitterActivity.this, "No Media on Tweet or Invalid Link", Toast.LENGTH_SHORT).show();
                });

            }
        }

        public void onError(Throwable th) {
            appProgressDialog.dismissDialog();
            th.printStackTrace();
        }

        public void onComplete() {
            appProgressDialog.dismissDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(TwitterActivity.this);
        if (isConnectedNetwork(TwitterActivity.this)) {
            MyApplication.activity = this;
            init();
            setUpEvents();
            AppUtils.getShowAdData(TwitterActivity.this, TAG, adContainer, adContainernative);
        } else {
            NoInternetConnection(TwitterActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        appIcon = (ImageView) findViewById(R.id.appIcon);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        appProgressDialog = new AppProgressDialog(TwitterActivity.this);
        commonClassForAPI = CommonClassForAPI.getInstance(TwitterActivity.this);
        AppUtils.createFileFolder();
    }

    private void setUpEvents() {
        txtTitle.setText("Twitter");
        txtAppTitle.setText("Twitter");
        appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_twitter));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_twitter_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_twitter_2));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }
            }
        });
        etUrl.setHint("Enter or Paste Twitter URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(TwitterActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                try {
                    AppUtils.createFileFolder();
                    String twitterVideoUrl = etUrl.getText().toString();
                    if (TextUtils.isEmpty(twitterVideoUrl)) {
                        etUrl.setError("Enter Twitter URL");
                    } else {
                        if (new URL(twitterVideoUrl).getHost().contains("twitter")) {
                            Long tweetId = getTweetId(twitterVideoUrl);
                            if (tweetId != null) {
                                callGetTwitterData(String.valueOf(tweetId));
                            }
                        } else {
                            etUrl.setError("Invalid Twitter URL");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public String getFilenameFromURL(String str, String str2) {
        if (str2.equals("image")) {
            try {
                return new File(new URL(str).getPath()).getName() + "";
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return System.currentTimeMillis() + ".jpg";
            }
        } else {
            try {
                return new File(new URL(str).getPath()).getName() + "";
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
                return System.currentTimeMillis() + ".mp4";
            }
        }
    }

    private void callGetTwitterData(String str) {
        if (this.commonClassForAPI != null) {
            appProgressDialog.showDialog();
            commonClassForAPI.callTwitterApi(this.observer, "https://twittervideodownloaderpro.com/twittervideodownloadv2/index.php", str);
        }
    }

    private Long getTweetId(String str) {
        try {
            return Long.valueOf(Long.parseLong(str.split("\\/")[5].split("\\?")[0]));
        } catch (Exception e) {
            return null;
        }
    }
}