package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.Constant.NoInternetConnection;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.Prefs;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.RetrofitUtils.CommonClassForAPI;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.view.AppProgressDialog;
import com.smtech.socialdownloader.view.OnSingleClickListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

public class RoposoActivity extends Activity {

    private EditText etUrl;
    private Button btnPaste,btnOKinsta;
    private Button btnDownload;
    private ImageView iconHelp, iconBack, appIcon, imgTutorial1, imgTutorial2,iconInsta;
    private TextView txtTitle, txtAppTitle;
    CommonClassForAPI commonClassForAPI;
    public String VideoUrl;
    AppProgressDialog appProgressDialog;
    boolean isShowTutorial = false;
    private CardView cardTutorial;
    LinearLayout adContainer, adContainernative;
    LinearLayout thirdBannerContainer;
    private static final String TAG = "RoposoActivity";

    ImageView imgTutorial_1,imgTutorial_2;
    Prefs prefs;
    ScrollView ropososcroll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(RoposoActivity.this);
        if (isConnectedNetwork(RoposoActivity.this)) {
            MyApplication.activity = this;
            prefs = new Prefs(this);
            init();
            setUpEvents();
            if(prefs.getPremium()==1){
                adContainer.setVisibility(View.GONE);
                adContainernative.setVisibility(View.GONE);
                thirdBannerContainer.setVisibility(View.GONE);
            }
            else {
                AppUtils.getShowAdData(RoposoActivity.this, TAG, adContainer, adContainernative);
                AppUtils.getShowThirdPartyAdData(RoposoActivity.this, TAG, thirdBannerContainer, null);
            }

        } else {
            NoInternetConnection(RoposoActivity.this);
        }

        ropososcroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = ropososcroll.getScrollY(); // For ScrollView
                int scrollX = ropososcroll.getScrollX(); // For HorizontalScrollView
                if(scrollX<scrollY){
                    adContainer.setVisibility(View.VISIBLE);
                }
                else  {
                    adContainer.setVisibility(View.GONE);
                }
            }
        });
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }*/

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
       // thirdBannerContainer = (LinearLayout) findViewById(R.id.third_banner_container);
        adContainernative = (LinearLayout) findViewById(R.id.native_container_insta);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnPaste = (Button) findViewById(R.id.btnPaste);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        iconHelp = (ImageView) findViewById(R.id.iconHelp);
        ropososcroll = findViewById(R.id.fb_scroll);
        txtTitle = (TextView) findViewById(R.id.txtTitle_fb);
        //  txtAppTitle = (TextView) findViewById(R.id.txtAppTitle);
        // appIcon = (ImageView) findViewById(R.id.appIcon);
        imgTutorial1 = (ImageView) findViewById(R.id.imgTutorial1);
        imgTutorial2 = (ImageView) findViewById(R.id.imgTutorial2);
        iconInsta = (ImageView) findViewById(R.id.iconInsta);
        cardTutorial = (CardView) findViewById(R.id.cardTutorial);
        appProgressDialog = new AppProgressDialog(RoposoActivity.this);
        commonClassForAPI = CommonClassForAPI.getInstance(RoposoActivity.this);
        imgTutorial_1 = (ImageView) findViewById(R.id.imgTutorial_1);
        imgTutorial_2 = (ImageView) findViewById(R.id.imgTutorial_2);
        btnOKinsta = (Button) findViewById(R.id.btnOKinsta_button);

        AppUtils.createFileFolder();
    }

    private void setUpEvents() {
        txtTitle.setText("Roposo");
        //  txtAppTitle.setText("Roposo");
        //  appIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_roposo));
        imgTutorial1.setImageDrawable(getResources().getDrawable(R.drawable.img_roposo_1));
        imgTutorial2.setImageDrawable(getResources().getDrawable(R.drawable.img_roposo_2));
        imgTutorial_1.setImageDrawable(getResources().getDrawable(R.drawable.img_roposo_1));
        imgTutorial_2.setImageDrawable(getResources().getDrawable(R.drawable.img_roposo_2));

        iconInsta.setImageDrawable(getResources().getDrawable(R.drawable.roposo_icon));
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnOKinsta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cardTutorial.setVisibility(View.GONE);
                    }
                });

                isShowTutorial = !isShowTutorial;
                if (isShowTutorial) {
                    cardTutorial.setVisibility(View.VISIBLE);
                } else {
                    cardTutorial.setVisibility(View.GONE);
                }

            }

        });
        etUrl.setHint("Enter or Paste Roposo URL");
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etUrl.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPaste.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip() && clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    etUrl.setText(item.getText().toString());
                } else {
                    Toast.makeText(RoposoActivity.this, "Copy text to paste!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDownload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String obj = etUrl.getText().toString();
                if (TextUtils.isEmpty(obj)) {
                    etUrl.setError("Enter Roposo URL");
                } else {
                    GetRoposoData(obj);
                }
            }
        });
    }

    private void GetRoposoData(String url) {
        try {
            AppUtils.createFileFolder();
            if (new URL(url).getHost().contains("roposo")) {
                appProgressDialog.showDialog();
                new callGetRoposoData().execute(url);
            } else {
                etUrl.setError("Invalid Roposo URL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class callGetRoposoData extends AsyncTask<String, Void, Document> {
        Document RoposoDoc;

        callGetRoposoData() {
        }

        public void onPreExecute() {
            super.onPreExecute();
        }

        public Document doInBackground(String... strArr) {
            try {
                this.RoposoDoc = Jsoup.connect(strArr[0]).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.RoposoDoc;
        }

        public void onPostExecute(Document document) {
            appProgressDialog.dismissDialog();
            try {
                VideoUrl = document.select("meta[property=\"og:video:url\"]").last().attr(FirebaseAnalytics.Param.CONTENT);
                if (!VideoUrl.equals("")) {
                    try {
                        String str = AppUtils.RootDirectoryRoposo;
                        AppUtils.startDownload(VideoUrl, str, RoposoActivity.this, "roposo_" + System.currentTimeMillis() + ".mp4");
                        VideoUrl = "";
                        etUrl.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
        }
    }
}