package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.ad_load_dialoog;
import static com.smtech.socialdownloader.utils.AdmobAdUtils.showAdmobInterstitialAdIntentCheck;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.utils.AppUtils;

public class ExitAppActivity extends AppCompatActivity {

    private LinearLayout YesLinearLayout, NoLinearLayout;
    private Intent intent;

    private static final String TAG = "ExitAppActivity";

    LinearLayout adContainer;
    LinearLayout adContainernative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit_app);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(ExitAppActivity.this);


        if (isConnectedNetwork(ExitAppActivity.this)) {

            adContainer = (LinearLayout) findViewById(R.id.banner_container);
          //  adContainernative = (LinearLayout) findViewById(R.id.native_container);
            AppUtils.getShowAdData(ExitAppActivity.this, TAG, adContainer, null);

            YesLinearLayout = (LinearLayout) findViewById(R.id.YesLinearLayout);
            NoLinearLayout = (LinearLayout) findViewById(R.id.NoLinearLayout);

            NoLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showAdmobInterstitialAdIntentCheck(ExitAppActivity.this, true, true);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (ad_load_dialoog != null) {
                                if (ad_load_dialoog.isShowing()) {
                                    handler.postDelayed(this, 100);

                                } else {
                                    /*intent = new Intent(ExitAppActivity.this, StartActivity.class);
                                    startActivity(intent);
                                    finish();*/
                                    onBackPressed();
                                }
                            } else {
                                /*intent = new Intent(ExitAppActivity.this, StartActivity.class);
                                startActivity(intent);
                                finish();*/
                                onBackPressed();
                            }
                        }
                    }, 100);
                }
            });

            YesLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showAdmobInterstitialAdIntentCheck(ExitAppActivity.this, true, true);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (ad_load_dialoog != null) {
                                if (ad_load_dialoog.isShowing()) {
                                    handler.postDelayed(this, 100);

                                } else {
                                    intent = new Intent(ExitAppActivity.this, ThanksActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                            } else {
                                intent = new Intent(ExitAppActivity.this, ThanksActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        }
                    }, 100);

                }
            });

        } else {

            NoInternetConnection(ExitAppActivity.this);

        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}