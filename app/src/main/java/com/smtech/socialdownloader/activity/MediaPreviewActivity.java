package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.SnapHelper;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;
import com.smtech.socialdownloader.adapter.MediaPreviewAdapter;
import com.smtech.socialdownloader.globle.AppMethod;
import com.smtech.socialdownloader.utils.AdmobAdUtils;
import com.smtech.socialdownloader.utils.AppUtils;
import com.smtech.socialdownloader.utils.FacebookAdUtils;
import com.smtech.socialdownloader.utils.FirebaseAnalyticsUtils;

import java.io.File;
import java.util.ArrayList;
import im.ene.toro.widget.Container;
import im.ene.toro.widget.PressablePlayerSelector;

public class MediaPreviewActivity extends AppCompatActivity {
    ArrayList<String> filePathList;
    int initPos;
    boolean fromWAStatus;
    Toolbar mToolbar;
    Container rvMedia;
    SnapHelper snapHelper;
    LinearLayoutManager layoutManager;
    PressablePlayerSelector selector;
    MediaPreviewAdapter adapter;
    private static final int PERMISSION_REQUEST_CODE = 1001;
    boolean updateFlag = false;
    LinearLayout adContainer;
    private static final String TAG = "MediaPreviewActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_preview);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(MediaPreviewActivity.this);
        if (isConnectedNetwork(MediaPreviewActivity.this)) {
            MyApplication.activity = this;
            initArguments();
            initActionBar();
            init();
            AppUtils.getShowAdData(MediaPreviewActivity.this, TAG, adContainer, null);
        } else {
            NoInternetConnection(MediaPreviewActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

    private void initArguments() {
        if (getIntent().hasExtra("filePathList")) {
            filePathList = getIntent().getStringArrayListExtra("filePathList");
            if (filePathList != null && filePathList.size() > 0) {
                initPos = getIntent().getIntExtra("initPos", 0);
                fromWAStatus = getIntent().getBooleanExtra("fromWAStatus", false);
            } else {
                MediaPreviewActivity.this.finish();
            }
        } else {
            MediaPreviewActivity.this.finish();
        }
    }

    private void initActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void init() {
        adContainer = (LinearLayout) findViewById(R.id.banner_container);
        rvMedia = (Container) findViewById(R.id.rvMedia);
        snapHelper = new PagerSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                return super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
            }
        };
        layoutManager = new LinearLayoutManager(MediaPreviewActivity.this);
        rvMedia.setLayoutManager(layoutManager);
        selector = new PressablePlayerSelector(rvMedia);
        rvMedia.setPlayerSelector(selector);
        snapHelper.attachToRecyclerView(rvMedia);
        rvMedia.setHasFixedSize(true);
        rvMedia.setItemViewCacheSize(3);
        rvMedia.setDrawingCacheEnabled(true);
        rvMedia.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        adapter = new MediaPreviewAdapter(MediaPreviewActivity.this, filePathList, selector, new MediaPreviewAdapter.MediaPreviewListener() {
            @Override
            public void shareClick(int position) {
                shareMedia(position);
            }

            @Override
            public void downloadClick(int position) {
                downloadMedia(position);
            }

            @Override
            public void deleteClick(int position) {
                deleteMedia(position);
            }
        });
        adapter.setFromWAStatus(fromWAStatus);
        rvMedia.setAdapter(adapter);
        if (rvMedia.getAdapter().getItemCount() > 0) {
            rvMedia.getLayoutManager().scrollToPosition(initPos);
        }
    }

    private void shareMedia(final int position) {
        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("*/*");
            File file = new File(filePathList.get(position));
            Uri files = Uri.fromFile(file);
            intent.putExtra(Intent.EXTRA_STREAM, files);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downloadMedia(final int position) {
        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(MediaPreviewActivity.this, PERMISSIONS)) {
            File file = new File(filePathList.get(position));

            if (file != null && file.exists()) {
                if (AppUtils.saveWhatsappStatus(MediaPreviewActivity.this, file)) {
                    FirebaseAnalyticsUtils.statusDownload(MediaPreviewActivity.this, AppUtils.isVideoFile(file.getAbsolutePath()), "WhatsApp");
                    AppMethod.showAlertWithOk(MediaPreviewActivity.this, getString(R.string.app_name), "File downloaded successfully!");
                    if (AppUtils.isAdmobAvailable(MediaPreviewActivity.this)) {
                        AdmobAdUtils.showAdmobInterstitialAd(MediaPreviewActivity.this, true, true);
                    } else {
                        FacebookAdUtils.showFbInterstitialAd(MediaPreviewActivity.this, true);
                    }
                } else {
                    AppMethod.showAlertWithOk(MediaPreviewActivity.this, getString(R.string.app_name), "File download error!");
                }
            } else {
                AppMethod.showAlert(MediaPreviewActivity.this, "File not exixts!");
            }
        } else {
            ActivityCompat.requestPermissions(MediaPreviewActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }
    }

    private void deleteMedia(final int position) {
        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (AppMethod.hasPermissions(MediaPreviewActivity.this, PERMISSIONS)) {
            AppMethod.showInfoDialog(MediaPreviewActivity.this, getString(R.string.app_name), "Are you sure you want to delete this file?", true, "Yes", "No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    File file = new File(filePathList.get(position));
                    if (file != null && file.exists()) {
                        if (file.delete()) {
                            updateFlag = true;
                            filePathList.remove(position);
                            adapter.notifyItemRemoved(position);
                            if (filePathList.size() == 0) {
                                FirebaseAnalyticsUtils.deleteStatus(MediaPreviewActivity.this, "MediaPreview");
                                Intent resultIntent = new Intent();
                                setResult(RESULT_OK, resultIntent);
                                MediaPreviewActivity.this.finish();
                            }
                        } else {
                            AppMethod.showAlertWithOk(MediaPreviewActivity.this, getString(R.string.app_name), "Error occured during delete this file!");
                        }
                    }
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            });
        } else {
            ActivityCompat.requestPermissions(MediaPreviewActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (AppMethod.isAllPermissionGranted(grantResults)) {
                    Toast.makeText(this, "Enable Permissions! Try now!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (updateFlag) {
            Intent resultIntent = new Intent();
            setResult(RESULT_OK, resultIntent);
            MediaPreviewActivity.this.finish();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        layoutManager = null;
        adapter = null;
        selector = null;
        super.onDestroy();
    }
}