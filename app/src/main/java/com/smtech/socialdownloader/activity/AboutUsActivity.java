package com.smtech.socialdownloader.activity;

import static com.smtech.socialdownloader.Constant.NoInternetConnection;
import static com.smtech.socialdownloader.Constant.isConnectedNetwork;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.smtech.socialdownloader.MyApplication;
import com.smtech.socialdownloader.PrefData;
import com.smtech.socialdownloader.R;

import java.lang.reflect.Field;

public class AboutUsActivity extends AppCompatActivity {

    private ImageView iconBack, imgCall, imgWhatsApp, imgSkype;
    private CardView cardEmail, cardPrivacy, cardWebSite;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_about_us);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new PrefData(AboutUsActivity.this);
        if (isConnectedNetwork(AboutUsActivity.this)) {
            MyApplication.activity = this;
            init();
            setUpEvents();
        } else {
            NoInternetConnection(AboutUsActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

    private void init() {
        cardEmail = (CardView) findViewById(R.id.cardEmail);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        imgCall = (ImageView) findViewById(R.id.imgCall);
        imgWhatsApp = (ImageView) findViewById(R.id.imgWhatsApp);
        imgSkype = (ImageView) findViewById(R.id.imgSkype);
        cardPrivacy = (CardView) findViewById(R.id.cardPrivacy);
        cardWebSite = (CardView) findViewById(R.id.cardWebSite);
    }

    private void setUpEvents() {
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        cardEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Field[] fields = Build.VERSION_CODES.class.getFields();
                String osName = fields[Build.VERSION.SDK_INT].getName();
                sendFeedback(osName);
            }
        });
        cardWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.web_site))));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.web_site))));
                }
            }
        });
        imgCall.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getResources().getString(R.string.contact)));
                startActivity(intent);
            }
        });
        imgWhatsApp.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                PackageManager packageManager = getPackageManager();
                try {
                    String url = "https://api.whatsapp.com/send?phone=" + getResources().getString(R.string.contact);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setType("text/plain");
                    intent.setPackage("com.whatsapp");
                    intent.setData(Uri.parse(url));
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent);
                    } else {
                        Toast.makeText(AboutUsActivity.this, "App not found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imgSkype.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                if (isSkypeClientInstalled()) {
                    Intent sky = new Intent(Intent.ACTION_VIEW);
                    sky.setData(Uri.parse("skype:" + getResources().getString(R.string.skype_username) + "?call&video=false"));
                    startActivity(sky);
                } else {
                    Toast.makeText(AboutUsActivity.this, "App not found.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cardPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutUsActivity.this, PolicyActivity.class);
                startActivity(intent);
            }
        });
    }

    private void sendFeedback(String osName) {
        String[] TO = {getString(R.string.feedback_mail_id)};
        String title = " Device : " + Build.DEVICE + " " + " Model : " + Build.MODEL + " " + " Android Version : " + osName;
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        emailIntent.putExtra(Intent.EXTRA_TEXT, title);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(AboutUsActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isSkypeClientInstalled() {
        PackageManager myPackageMgr = getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }
}
